
clear legend
clear xlabel
clear ylabel

figure;
hold on

plot(ROCArray(1,:),'color',cc(1,:),'LineWidth',5);
plot(ROCArray(2,:),'color',cc(2,:),'LineWidth',5);
plot(ROCArray(3,:),'color',cc(3,:),'LineWidth',5);
plot(ROCArray(4,:),'color',cc(4,:),'LineWidth',5);
plot(ROCArray(11,:),'color',cc(5,:),'LineWidth',5);
plot(ROCArray(5,:),'color',cc(6,:),'LineWidth',5);
plot(ROCArray(6,:),'color',cc(7,:),'LineWidth',5);
plot(ROCArray(7,:),'color',cc(8,:),'LineWidth',5);
plot(ROCArray(8,:),'color',cc(9,:),'LineWidth',5);
plot(ROCArray(9,:),'color',cc(10,:),'LineWidth',5);
plot(ROCArray(10,:),'color',cc(11,:),'LineWidth',5);

legend = legend(...
    'Age',...
    'PSA',...
    'Gleason Score',...
    'T2 weighted',...
    'T2 un-weighted',...
    'ADC Philips',...
    'ACD Plugin',...
    'ADC normalized',...
    'KT',...
    'VE',...
    'PET',...
    'Location','southeast');
set(legend,'FontSize',18);
xlabel = xlabel('False positive rate (1 - specificity)');
ylabel = ylabel('True positive rate (sensitivity)');
title1 = title('ROC Curve - Linear Regression');
set(title1,'FontSize',25);
set(xlabel,'FontSize',25);
set(ylabel,'FontSize',25);
set(gca,'XTickLabel',linspace(0,1,11))
