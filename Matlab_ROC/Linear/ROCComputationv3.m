
clear

% import data
data = importfile('./../data/totalData.csv', 2, 185);
data(:,23) = dataset(double(data(:,11))./double(data(:,18)));

% path
attributesIndex = [2,3,6,11,12,13,14,15,16,17,23];
classIndex = 20;

% params
link = {'identity','log','logit','probit','comploglog','reciprocal','loglog'};
distr = {'binomial','gamma','inverse gaussian','normal','poisson'};

% to keep
linkbest ='';
distrbest = '';
ROCbest = [];
AUCbest = 0;

attributes = double(data(2:end,attributesIndex));
tmp = attributes(:,11);
attributes(:,11) = attributes(:,10);
attributes(:,10) = attributes(:,9);
attributes(:,9) = attributes(:,8);
attributes(:,8) = attributes(:,7);
attributes(:,7) = attributes(:,6);
attributes(:,6) = attributes(:,5);
attributes(:,5) = tmp;

perm = randperm(size(attributes,1));
attributesNames = {
    'Age',...
    'PSA',...
    'Gleason Score',...
    'T2 un-weighted',...
    'T2 weighted',...
    'ADC Philips',...
    'ACD Plugin',...
    'ADC Normalized',...
    'KT',...
    'VE',...
    'PET',...
    'Total',...
    'Random Predictor'
    };
ROCArray = zeros(13,100);
AUCArray = zeros(13,1);

%% best params

for i=1:size(link,2)
    for j=1:size(distr,2)
        
        attributes = double(data(2:end,attributesIndex));
        classes = double(data(2:end,classIndex));
        
        if strcmp(distr{j},'binomial') || strcmp(distr{j},'poisson')
            classes = (classes+1)/2;
        elseif strcmp(distr{j},'gamma') || strcmp(distr{j},'inverse gaussian')
            classes = (classes+3)/2;
        end
        
        % permutation
        attributesPerm = attributes(perm,:);
        classesPerm = classes(perm,:);

        % predictions
        predictions = zeros(1,size(attributes,1));

        % test train
        for k=1:20
            testIndex = (k-1)*9+1:k*9;
            trainIndex = setdiff(1:size(attributes,1),testIndex);
            testx = attributesPerm(testIndex,:);
            testy = classesPerm(testIndex);
            trainx = attributesPerm(trainIndex,:);
            trainy = classesPerm(trainIndex,:);
            model = glmfit(trainx, trainy, distr{j}, 'link', link{i});
            predictions(testIndex) = glmval(model, testx, link{i});
        end

        predictions = real(predictions);
        
        if (~isnan(predictions(1))) 
            [AUC,ROC] = getAUCandROC(predictions,classes);
            display([num2str(AUC),' ',num2str(AUCbest)]);
            if AUC > AUCArray(12)
                ROCArray(12,:) = ROC;
                AUCArray(12) = AUC;
            end
        end
    end
end


%% per attribut




for i=1:size(link,2)
    for j=1:size(distr,2)
        for k=1:size(attributes,2)

            % classes attributes
            attributes = double(data(2:end,attributesIndex));
            classes = double(data(2:end,classIndex));

            if strcmp(distr{j},'binomial') || strcmp(distr{j},'poisson')
                classes = (classes+1)/2;
            elseif strcmp(distr{j},'gamma') || strcmp(distr{j},'inverse gaussian')
                classes = (classes+3)/2;
            end

            % permutation
            attributesPerm = attributes(perm,:);
            classesPerm = classes(perm,:);

            % predictions
            predictions = zeros(1,size(attributes,1));

            % test train
            for l=1:20
                testIndex = (l-1)*9+1:l*9;
                trainIndex = setdiff(1:size(attributes,1),testIndex);
                testx = attributesPerm(testIndex,k);
                testy = classesPerm(testIndex);
                trainx = attributesPerm(trainIndex,k);
                trainy = classesPerm(trainIndex,:);
                model = glmfit(trainx, trainy, distr{j}, 'link', link{i});
                predictions(testIndex) = glmval(model, testx, link{i});
            end
    
            predictions = real(predictions);

            if (~isnan(predictions(1))) 
                [AUC,ROC] = getAUCandROC(predictions,classes);
                display([num2str(AUC),' ',num2str(AUCbest)]);
                if AUC > AUCArray(k)
                    AUCArray(k) = AUC;
                    ROCArray(k,:) = ROC;
                end
            end
    
        end
    end
end

%% other attributes


ROCArray(13,:) = linspace(0,1,size(ROC,2));




%% draw attributes

colorHSV=hsv(13);

clear legend
clear xlabel
clear ylabel

figure;
hold on

plot(ROCArray(1,:),'color',colorHSV(1,:),'LineWidth',5);
plot(ROCArray(2,:),'color',colorHSV(2,:),'LineWidth',5);
plot(ROCArray(3,:),'color',colorHSV(3,:),'LineWidth',5);
plot(ROCArray(4,:),'color',colorHSV(4,:),'LineWidth',5);
plot(ROCArray(5,:),'color',colorHSV(5,:),'LineWidth',5);
plot(ROCArray(6,:),'color',colorHSV(6,:),'LineWidth',5);
plot(ROCArray(7,:),'color',colorHSV(7,:),'LineWidth',5);
plot(ROCArray(8,:),'color',colorHSV(8,:),'LineWidth',5);
plot(ROCArray(9,:),'color',colorHSV(9,:),'LineWidth',5);
plot(ROCArray(10,:),'color',colorHSV(10,:),'LineWidth',5);
plot(ROCArray(11,:),'color',colorHSV(11,:),'LineWidth',5);

legend = legend(...
    attributesNames{1},...
    attributesNames{2},...
    attributesNames{3},...
    attributesNames{4},...
    attributesNames{5},...
    attributesNames{6},...
    attributesNames{7},...
    attributesNames{8},...
    attributesNames{9},...
    attributesNames{10},...
    attributesNames{11},...
    'Location','southeast');
set(legend,'FontSize',18);
xlabel = xlabel('False positive rate (1 - specificity)');
ylabel = ylabel('True positive rate (sensitivity)');
title1 = title('ROC Curve - Linear Regression');
set(title1,'FontSize',25);
set(xlabel,'FontSize',25);
set(ylabel,'FontSize',25);
set(gca,'XTickLabel',linspace(0,1,11))


%% draw total

clear legend
clear xlabel
clear ylabel

figure;
hold on

plot(ROCArray(12,:),'color',colorHSV(12,:),'LineWidth',5);

legend = legend('Total','Location','southeast');
set(legend,'FontSize',18);
xlabel = xlabel('False positive rate (1 - specificity)');
ylabel = ylabel('True positive rate (sensitivity)');
title1 = title('ROC Curve - Linear Regression');
set(title1,'FontSize',25);
set(xlabel,'FontSize',25);
set(ylabel,'FontSize',25);
set(gca,'XTickLabel',linspace(0,1,11))




%% ttest

ttestArray = zeros(13,13);
for i = 1:13
    for j = 1:13
        [h,p] = ttest2(ROCArray(i,:),ROCArray(j,:));
        ttestArray(i,j) = p;
    end
end


clear legend
clear xlabel
clear ylabel

figure;
imagesc(ttestArray);
set(gca,'XTick', 1:13)
set(gca,'YTick', 1:13)
set(gca,'XTickLabel',attributesNames)
set(gca,'YTickLabel',attributesNames)
set(gca,'FontSize',25)
title1 = title('ROC Curve - Linear Regression');
