function [AUC,ROC] = getAUCandROC(predictions,classes)

    minClasses = min(classes);
    maxClasses = max(classes);
    steps = linspace(minClasses,maxClasses,100);
    
    positiveClasses = classes == maxClasses;
    positiveClassesNb = sum(positiveClasses);

    correctArray = zeros(1,size(steps,2));
    
    for i=1:size(steps,2)
        positivePredictions = predictions <= steps(i);
        positiveClasses = classes == maxClasses;
                
        correctNb = 0;
        for j=1:size(positiveClasses,1)
            if positiveClasses(j) == 1 && positivePredictions(j) == 1
                correctNb = correctNb + 1;
            end
        end
        correctArray(i) = correctNb;
    end
    
    ROC = correctArray ./ positiveClassesNb;
    ROC(1) = 0;
    ROC(end) = 1;
    AUC = sum(correctArray ./ (positiveClassesNb * size(correctArray,2)));
end