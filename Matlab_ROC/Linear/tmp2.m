clear legend
clear xlabel
clear ylabel

%% draw

figure;
hold on

plot(ROCArray(12,:),'color',cc(12,:),'LineWidth',5);

legend = legend('Total','Location','southeast');
set(legend,'FontSize',18);
xlabel = xlabel('False positive rate (1 - specificity)');
ylabel = ylabel('True positive rate (sensitivity)');
title1 = title('ROC Curve - Linear Regression');
set(title1,'FontSize',25);
set(xlabel,'FontSize',25);
set(ylabel,'FontSize',25);
set(gca,'XTickLabel',linspace(0,1,11))

