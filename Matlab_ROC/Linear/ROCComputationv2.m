
clear

% import data
data = importfile('./../data/totalData.csv', 2, 185);
data(:,23) = dataset(double(data(:,11))./double(data(:,18)));

% path
attributesIndex = [2,3,6,11,12,13,14,15,16,17,23];
classIndex = 20;

% params
link = {'identity','log','logit','probit','comploglog','reciprocal','loglog'};
distr = {'binomial','gamma','inverse gaussian','normal','poisson'};

% to keep
linkbest ='';
distrbest = '';
ROCbest = [];
AUCbest = 0;

attributes = double(data(2:end,attributesIndex));
perm = randperm(size(attributes,1));


%% best params

for i=1:size(link,2)
    for j=1:size(distr,2)
        
        attributes = double(data(2:end,attributesIndex));
        classes = double(data(2:end,classIndex));
        
        if strcmp(distr{j},'binomial') || strcmp(distr{j},'poisson')
            classes = (classes+1)/2;
        elseif strcmp(distr{j},'gamma') || strcmp(distr{j},'inverse gaussian')
            classes = (classes+3)/2;
        end
        
        % permutation
        attributesPerm = attributes(perm,:);
        classesPerm = classes(perm,:);

        % predictions
        predictions = zeros(1,size(attributes,1));

        % test train
        for k=1:20
            testIndex = (k-1)*9+1:k*9;
            trainIndex = setdiff(1:size(attributes,1),testIndex);
            testx = attributesPerm(testIndex,:);
            testy = classesPerm(testIndex);
            trainx = attributesPerm(trainIndex,:);
            trainy = classesPerm(trainIndex,:);
            model = glmfit(trainx, trainy, distr{j}, 'link', link{i});
            predictions(testIndex) = glmval(model, testx, link{i});
        end

        predictions = real(predictions);
        
        if (~isnan(predictions(1))) 
            [AUC,ROC] = getAUCandROC(predictions,classes);
            display([num2str(AUC),' ',num2str(AUCbest)]);
            if AUC > AUCbest
                AUCbest = AUC;
                ROCbest = ROC;
                linkbest = link{i};
                distrbest = distr{j};
            end
        end
    end
end


%% per attribut

attributesNames = {'age','psa','gleason','t2 un-weighted','adcPhilips','adcPlugin','adc','kt','ve','pet','t2 weighted'};
ROCArray = zeros(12,size(ROCbest,2));
AUCArray = zeros(13,1);

for i=1:size(attributes,2)

    % classes attributes
    attributes = double(data(2:end,attributesIndex));
    classes = double(data(2:end,classIndex));
    if strcmp(distrbest,'binomial') || strcmp(distrbest,'poisson')
        classes = (classes+1)/2;
    elseif strcmp(distrbest,'gamma') || strcmp(distrbest,'inverse gaussian')
        classes = (classes+3)/2;
    end
    
    % permutation
    attributesPerm = attributes(perm,:);
    classesPerm = classes(perm,:);
    
    % predictions
    predictions = zeros(1,size(attributes,1));
    
    % test train
    for k=1:20
        testIndex = (k-1)*9+1:k*9;
        trainIndex = setdiff(1:size(attributes,1),testIndex);
        testx = attributesPerm(testIndex,i);
        testy = classesPerm(testIndex);
        trainx = attributesPerm(trainIndex,i);
        trainy = classesPerm(trainIndex,:);
        model = glmfit(trainx, trainy, distrbest, 'link', linkbest);
        predictions(testIndex) = glmval(model, testx, linkbest);
    end
    
    predictions = real(predictions);
    [AUC,ROC] = getAUCandROC(predictions,classesPerm);
    ROCArray(i,:) = ROC;
    AUCArray(i) = AUC;
end

%% other attributes

ROCArray(12,:) = ROCbest;
AUCArray(12) = AUCbest;


ROCArray(13,:) = linspace(0,1,size(ROC,2));


cc=hsv(13);


%% draw

figure;
hold on

plot(ROCArray(1,:),'color',cc(1,:),'LineWidth',5);
plot(ROCArray(2,:),'color',cc(2,:),'LineWidth',5);
plot(ROCArray(3,:),'color',cc(3,:),'LineWidth',5);
plot(ROCArray(4,:),'color',cc(4,:),'LineWidth',5);
plot(ROCArray(11,:),'color',cc(5,:),'LineWidth',5);
plot(ROCArray(5,:),'color',cc(6,:),'LineWidth',5);
plot(ROCArray(6,:),'color',cc(7,:),'LineWidth',5);
plot(ROCArray(7,:),'color',cc(8,:),'LineWidth',5);
plot(ROCArray(8,:),'color',cc(9,:),'LineWidth',5);
plot(ROCArray(9,:),'color',cc(10,:),'LineWidth',5);
plot(ROCArray(10,:),'color',cc(11,:),'LineWidth',5);

legend = legend([attributesNames{1},', AUC = ',num2str(AUCArray(1))],...
    [attributesNames{2},', AUC = ',num2str(AUCArray(2))],...
    [attributesNames{3},', AUC = ',num2str(AUCArray(3))],...
    [attributesNames{4},', AUC = ',num2str(AUCArray(4))],...
    [attributesNames{11},', AUC = ',num2str(AUCArray(11))],...
    [attributesNames{5},', AUC = ',num2str(AUCArray(5))],...
    [attributesNames{6},', AUC = ',num2str(AUCArray(6))],...
    [attributesNames{7},', AUC = ',num2str(AUCArray(7))],...
    [attributesNames{8},', AUC = ',num2str(AUCArray(8))],...
    [attributesNames{9},', AUC = ',num2str(AUCArray(9))],...
    [attributesNames{10},', AUC = ',num2str(AUCArray(10))]...
    ,'Location','southeast');
set(legend,'FontSize',18);
xlabel = xlabel('False positive rate (1 - specificity)');
ylabel = ylabel('True positive rate (sensitivity)');
title1 = title(['ROC Curve - Linear Regression (',distrbest,' ',linkbest,')']);
set(title1,'FontSize',25);
set(xlabel,'FontSize',25);
set(ylabel,'FontSize',25);
set(gca,'XTickLabel',linspace(0,1,11))


clear legend
clear xlabel
clear ylabel

%% draw

figure;
hold on

plot(ROCArray(12,:),'color',cc(12,:),'LineWidth',5);

legend = legend(['total',', AUC = ',num2str(AUCArray(12))],'Location','southeast');
set(legend,'FontSize',18);
xlabel = xlabel('False positive rate (1 - specificity)');
ylabel = ylabel('True positive rate (sensitivity)');
title1 = title(['ROC Curve - Linear Regression (',distrbest,' ',linkbest,')']);
set(title1,'FontSize',25);
set(xlabel,'FontSize',25);
set(ylabel,'FontSize',25);
set(gca,'XTickLabel',linspace(0,1,11))










