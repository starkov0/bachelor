\enlargethispage {\baselineskip }
\select@language {american}
\contentsline {section}{\numberline {I}Acknowledgments}{3}{section.0.1}
\contentsline {section}{\numberline {II}Introduction}{3}{section.0.2}
\contentsline {section}{\numberline {III}Data}{4}{section.0.3}
\contentsline {subsection}{\numberline {1}Feature Extracted Data}{5}{subsection.0.3.1}
\contentsline {subsection}{\numberline {2}Raw imaging Data}{7}{subsection.0.3.2}
\contentsline {section}{\numberline {IV}Mining}{8}{section.0.4}
\contentsline {subsection}{\numberline {1}Data Mining}{8}{subsection.0.4.1}
\contentsline {subsubsection}{Results}{9}{table.0.1}
\contentsline {subsubsection}{Discussion}{10}{figure.0.13}
\contentsline {subsection}{\numberline {2}Model Mining}{11}{subsection.0.4.2}
\contentsline {subsubsection}{Results}{12}{table.0.3}
\contentsline {subsubsection}{Discussion}{15}{figure.0.16}
\contentsline {subsection}{\numberline {3}Convolutional Networks}{15}{subsection.0.4.3}
\contentsline {subsubsection}{Results}{16}{figure.0.17}
\contentsline {subsubsection}{Discussion}{16}{figure.0.17}
\contentsline {section}{\numberline {V}Conclusion}{16}{section.0.5}
