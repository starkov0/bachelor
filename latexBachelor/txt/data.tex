\section{Data}



Data analyzed is based on 23 male patients aged from 50 to 80 resident in Geneva, Switzerland. They all have been diagnosed and treated in the Geneva University Hospital (\textit{HUG}) during in 2013. Each patient went through blood screening, medical imaging, biopsy and finally radical prostatectomy. No patient without prostatectomy was included in the data set because his final diagnosis is for the moment uncertain. Final diagnosis is based on prostate's histopathology.

Here is an example of a post-prostatectomy histopathological diagnosis:

\begin{center}
	\begingroup
		\centering
		\includegraphics[scale=0.8]{../pics/histopatho}
		\captionof{figure}{"Whole prostate histopathological image. Encircled parts confirm malign cancer tissue."}
	\endgroup
\end{center}

Histopathological examination divides prostate tissue into several parts and examines each part in order to give a final diagnosis, because there might be multiple cancer lesions. In our case, the organ was divided into 8 parts. First division separates apical periphery, medial periphery, basal periphery and transition zones. Second division cuts left and right zones.

Here is an example of the divisions performed during histopathology:

\begin{center}
	\begingroup
		\centering
		\includegraphics[scale=0.4]{../pics/histopathoDivision}
		\captionof{figure}{"Prostate division. The red part corresponds to the transition zone. The gray parts are in the peripheral part. Left and right divisions are also displayed."}
	\endgroup
\end{center}

From the 23 initial patients, 184 prostate diagnoses were created. Each diagnosis is represented via a Boolean variable, cancer or no-cancer. There were 53 cancer instances and 131 no-cancer instances, which is 28\% of cancer instances and 72\% of no-cancer instances. Each patient may have no-cancer, mono-cancer or multiple-cancer instances.

In order to correlate with histopathological classes, attributes were created with the correspondence of the organ divisions. 

Attributes are represented by two different data sets. One is composed of feature extracted imaging and non-imaging data. The second one is composed of raw axial sliced DIMCOM images.

Classes, for the two data sets, are exactly the same. No modification on the histopathological results has been applied.



\subsection{Feature Extracted Data}

Feature extracted attributes are composed of patient's :

\begin{itemize}
	\item \textbf{Age}
	\item \textbf{PSA} (\textit{Prostate Specific Antigene}) blood concentration
	\item \textbf{Gleason score}
	\item \textbf{T2 un-weighted}
	\item \textbf{T2 weighted}
	\item  $\mathbf{ADC_{Philips}}$, Apparent Diffusion Coefficient value measured with a Philips$\copyright$ imaging workstation
	\item  $\mathbf{ADC_{Plugin}}$, Apparent Diffusion Coefficient value measured with an Osirix$\copyright$ Plugin software
	\item  $\mathbf{ADC_{Normalized}}$, Apparent Diffusion Coefficient normalized with the Osirix$\copyright$ Plugin software
	\item \textbf{KT}, forward volume transfer constant
	\item \textbf{VE}, interstitial volume fraction
	\item \textbf{PET}
\end{itemize}

PSA is a protein produced by prostate's gland cells. Combined with patient's age, it's blood concentration is a common tool for prostate cancer screening. \citep{carlson1998algorithm}

Gleason score is a post-biopsy histopathological score. It gives a great idea of cancer likelihood. 

T2, ADC, KT and VE are all magnetic resonance imaging features. They are computed by analyzing the mean or max value in a local region of interest. T2 is a sequence for basic visualization. ADC corresponds to water diffusion inside a region. KT corresponds to the water inside the vascular system inside a region of interest. VE corresponds to the water inside the tissue and outside the cells and the vascular system. \citep{ewing2013model}

PET is a nuclear imaging modality. \citep{hara1998pet} Just like MRI features, it's value is a mean or max value in a local region of interest.

Here are examples of MRI T2, ADC and PET images: 

\begin{center}
	\begingroup
		\centering
		\includegraphics[scale=0.8]{../pics/MRIT2}
		\captionof{figure}{"Pelvic MRI T2 sequence."}
	\endgroup
\end{center}

\begin{center}
	\begingroup
		\centering
		\includegraphics[scale=0.72]{../pics/ADC}
		\captionof{figure}{"Pelvic ADC image."}
	\endgroup
\end{center}

\begin{center}
	\begingroup
		\centering
		\includegraphics[scale=0.75]{../pics/PET}
		\captionof{figure}{"Pelvic PET-scan image."}
	\endgroup
\end{center}

Classes are histopathological diagnoses, on which no modification has been applied.

A licensed radiologist, Thomas De Perrot at the Department of Radiology of the University Hospital of Geneva, Switzerland, in 2014, has created this set.



\subsection{Raw imaging Data}

In order to create predictions from raw imaging data, there is a need for pre-processing. It includes segmentation, normalization and data set enlargement. \citep{krizhevsky2012imagenet} 

As manual segmentation is time consuming, only one modality has been used; pelvic T2 MRI with endorectal coil, 784 x 784 DICOM images. This type of images have been chosen because, from all image types there were in the data set, they allowed the best visual details visualization. Here is an example of pre-processed images:

\begin{center}
	\begingroup
		\centering
		\includegraphics[scale=0.58]{../pics/prostateEndoCoil}
		\captionof{figure}{"Endorectal coil T2 MRI DICOM image."}
	\endgroup
\end{center}

Segmentation has been performed based on histopathological divisions. Non-prostate tissue pixels have been set to zeros. For un-necessery data size reduction, segmented protstate's tissue has been centered, non-necessary parts have been removed. Final images' size is 256 x 256.

Normalization, via setting mean image's mean value to 0 and standard deviation to 1 has been applied.

A 5th year medical student, my-self, and validated by a licensed radiologist, Thomas De Perrot, at the University Hospital of Geneva, Switzerland, has manually performed segmentation.

Here is an example of the post-segmentation images: 

\begin{center}
	\begingroup
		\includegraphics[scale=1.01]{../pics/RA}
		\includegraphics[scale=1.0]{../pics/LA}
		\captionof{figure}{"Prostate's right and left apex zone."}
	\endgroup
\end{center}

\begin{center}
	\begingroup
		\centering
		\includegraphics[scale=1.01]{../pics/RM}
		\includegraphics[scale=1.0]{../pics/LM}
		\captionof{figure}{"Prostate's right and left medial zone."}
	\endgroup
\end{center}

\begin{center}
	\begingroup
		\centering
		\includegraphics[scale=1.01]{../pics/RB}
		\includegraphics[scale=1.0]{../pics/LB}
		\captionof{figure}{"Prostate's right and left basal zone."}
	\endgroup
\end{center}

\begin{center}
	\begingroup
		\centering
		\includegraphics[scale=0.74]{../pics/RT}
		\includegraphics[scale=0.8]{../pics/LT}
		\captionof{figure}{"Prostate'sright and left transition zone."}
	\endgroup
\end{center}

Data enlargement, allowing overfitting reduction, has been performed via rotations. \citep{krizhevsky2012imagenet} \citep{howard2013some} 5° angles ahs been used for this part. Final data set contains 13248 images.

Classes are histopathological diagnoses, on which no modification has been applied.