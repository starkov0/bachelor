


library(geepack)
library(ROCR)
library(gdata)
library(pROC)


library(coin)
library(clinfun)

######


pdf("zonePeriph.pdf")
tiff(filename = "figure", width = 480, height = 480, units = "px", compression="none", bg = "white")

dev.off()

#### importation du tableau de donn�es
# LINUX

d = read.table("test.csv", header=T, sep=",")
d[,"gleason"] = paste(d[,5], "et", d[,6])
d =  d[d[,"seg"]!=c("ZT D", "ZT G"),]
# d =  d[d[,"seg"]==c("ZT D", "ZT G"),]
d[,"seg"] = as.character(d[,"seg"])

######## Gold standard

d[ d[,"pathoM"]=="n"    , "target" ]  = 0
d[ d[,"pathoM"]=="M"    , "target"  ] = 0  
d[ d[,"pathoM"]=="PIN"  , "target"  ] = 0   
d[ d[,"pathoM"]=="E25"  , "target"  ] = 0  
d[ d[,"pathoM"]=="E50"  , "target"  ] = 0
d[ d[,"pathoM"]=="Fc"    , "target" ] = 1  
d[ d[,"pathoM"]=="Fp"   , "target"  ] = 1  

#### normalisation des t2

d["t2"] = d["t2"] / d["pn"] 


##########################################################
# MAC

d =read.xls("test.xls")
d[,"gleason"] = paste(d[,5], "et", d[,6])
#d =  d[d[,"seg"]!=c("ZT D", "ZT G"),]
# d =  d[d[,"seg"]==c("ZT D", "ZT G"),]
d[,"seg"] = as.character(d[,"seg"])

######## Gold standard

d[ d[,"pathoM"]=="n"    , "target" ]  = 0
d[ d[,"pathoM"]=="M"    , "target"  ] = 0  
d[ d[,"pathoM"]=="PIN"  , "target"  ] = 0   
d[ d[,"pathoM"]=="E25"  , "target"  ] = 0  
d[ d[,"pathoM"]=="E50"  , "target"  ] = 0
d[ d[,"pathoM"]=="Fc"    , "target" ] = 1  
d[ d[,"pathoM"]=="Fp"   , "target"  ] = 1  

#### normalisation des t2

d["t2"] = d["t2"] / d["pn"] 


##########################################################




d =  d[d[,"target"]!=1,]


##### controle des donnees



ncol= "pet"

p1 = which(d["seg"]=="ZT D" | d["seg"]=="ZT G")
p0 = which(d["seg"]=="Base D" | d["seg"]=="Base G"  | d["seg"]=="Moyen D" | d["seg"]=="Moyen G" | d["seg"]=="Apex D" | d["seg"]=="Apex G"  )

x1 = d[p1, ncol]
x0 = d[p0, ncol]

boxplot(x0,x1, names=c("zone p�riph�rique","zone de transition"))
commentaire = "microfoyer exclu, apres normalisation t2"
title(paste(ncol, commentaire))

options(digits=3)

summary(x0)
cat( median(x0),"(", min(x0), "-", max(x0), ")", sep="")

summary(x1)
cat( median(x1),"(", min(x1), "-", max(x1), ")", sep="")


wilcox.test(x0,x1)   #nonparametric test







commentaire = "microfoyer exclu, apres normalisation t2"
title(paste(ncol, commentaire))

shapiro.test(x0)
shapiro.test(x1)
var.test(x0, x1)                  # Do x and y have the same variance?
t.test(x0,x1, var.equal=T)   #parametric test









