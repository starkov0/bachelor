import xlrd
from numpy import array
import csv
import copy

def writeCSV(name, data):
	ar = array(data)
	fl = open(name, 'w')
	writer = csv.writer(fl)
	writer.writerow(ar[0]) #if needed
	for values in ar[1:len(ar)+1]:
		writer.writerow(values)
	fl.close()  

def readCSV(name):
	data = []
	wb = xlrd.open_workbook(name)
	wb.sheet_names()
	sh = wb.sheet_by_index(0)
	for rownum in range(sh.nrows):
		data.append(sh.row_values(rownum))
	return data

#################################################################

name = "PETIRM_121105_v50_realNames.xls"
data = readCSV(name)

dataModified = copy.deepcopy(data)

for row in dataModified:
	if row[18] == "Fp" or row[18] == "Fc":
		row[19] = 1.0
	else:
		row[19] = -1.0

writeCSV("totalData.csv",dataModified)

