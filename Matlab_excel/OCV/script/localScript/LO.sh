#!/bin/bash
cd ..
cd ..
cd codeOCV
cd Logistic
while read LINE
do
	array=(${LINE// / })
	echo "MAINLO('${array[0]}','${array[1]}',${array[2]})" | /Applications/Matlab.app/bin/matlab -nodesktop -nosplash -singleCompThread
done < ../../paramIndexOCV/Logistic.txt