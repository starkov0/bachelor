% DUMMY

function MAINDU(outterTestIndex)

    % init data
    Current_dir = './../../dataOCV/Dummy/';

    % import data
    data = importfile('./../../data/totalData.csv', 2, 185);
    data(:,23) = dataset(double(data(:,11))./double(data(:,18)));

    % path
    param_name = num2str(outterTestIndex);
    dir_path = strcat(Current_dir,param_name,'/');
    file_path = strcat(dir_path,'outterCVdata.mat');

    if not(exist(dir_path,'dir'))
        mkdir(dir_path);
    end

    display(['Start : ',file_path]);

    % check if the file exists
    if not(exist(file_path,'file'))
        output = computePredictions(data,outterTestIndex);
        save(file_path,'output');
    end

    display(['End : ',file_path]);

end
