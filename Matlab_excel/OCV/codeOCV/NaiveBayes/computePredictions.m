% NAIVE BAYES

% computerPredictions Based on Naive Bayes Algorithm
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function output = computePredictions(data,Distr,Prior,KSSupport,KSType,outterTestIndex)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    % init data
    initDataCompute;

    % outter test - train
    outterTrainIndex = setdiff(outterInstancesList,outterTestIndex);

    % train - test -> INSTANCES
    outterTrainAttributes = double(data(outterTrainIndex,attributes));
    outterTestAttributes = double(data(outterTestIndex,attributes));
    outterTrainClasses = (double(data(outterTrainIndex,classe)));
    outterTestClasses = (double(data(outterTestIndex,classe)));

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % modify classes type
    outterTrainClasses = (outterTrainClasses+1)/2;
    outterTestClasses = (outterTestClasses+1)/2;

    % predictions
    try
        B = NaiveBayes.fit(outterTrainAttributes,outterTrainClasses,'Distribution',Distr,'Prior',Prior,'KSSupport',KSSupport,'KSType',KSType);
        outterPredictedProbaTOT = posterior(B,outterTestAttributes);
        outterPredictedProba = outterPredictedProbaTOT(:,2);
    catch err
        outterPredictedProba = ones(size(outterTestClasses))*-100;
    end
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    output = {outterTestIndex,outterTestClasses,outterPredictedProba};

end