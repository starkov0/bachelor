% DECISION TREE CLASSIFICATION

% computerPredictions Based on Naive Bayes Algorithm
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function output = computePredictions(data,Merge,MinLeaf,MinParent,prior,Prune,PruneC,Split,Surrogate,outterTestIndex)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    % init data
    initDataCompute;

    % outter test - train
    outterTrainIndex = setdiff(outterInstancesList,outterTestIndex);

    % train - test -> INSTANCES
    outterTrainAttributes = double(data(outterTrainIndex,attributes));
    outterTestAttributes = double(data(outterTestIndex,attributes));
    outterTrainClasses = (double(data(outterTrainIndex,classe)));
    outterTestClasses = (double(data(outterTestIndex,classe)));

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % predictions
    try
        B = ClassificationTree.fit(outterTrainAttributes,outterTrainClasses,'MergeLeaves',Merge,'MinLeaf',MinLeaf,'MinParent',MinParent,'prior',prior,'Prune',Prune,'PruneCriterion',PruneC,'SplitCriterion',Split,'Surrogate',Surrogate);
        outterPredictedProba = predict(B,outterTestAttributes);
    catch err
        outterPredictedProba = ones(size(outterTestClasses))*-100;
    end
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    output = {outterTestIndex,outterTestClasses,outterPredictedProba};

end