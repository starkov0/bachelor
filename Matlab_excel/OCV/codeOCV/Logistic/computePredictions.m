% LOGISTIC REGRESSION

% computerPredictions Based on Naive Bayes Algorithm
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function output = computePredictions(data,link,distr,outterTestIndex)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    % init data
    initDataCompute;

    % outter test - train
    outterTrainIndex = setdiff(outterInstancesList,outterTestIndex);

    % train - test -> INSTANCES
    outterTrainAttributes = double(data(outterTrainIndex,attributes));
    outterTestAttributes = double(data(outterTestIndex,attributes));
    outterTrainClasses = (double(data(outterTrainIndex,classe)));
    outterTestClasses = (double(data(outterTestIndex,classe)));

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % modify classes type
    if strcmp(distr,'binomial') || strcmp(distr,'poisson')
        outterTrainClasses = (outterTrainClasses+1)/2;
        outterTestClasses = (outterTestClasses+1)/2;
    elseif strcmp(distr,'gamma') || strcmp(distr,'inverse gaussian')
        outterTrainClasses = (outterTrainClasses+3)/2;
        outterTestClasses = (outterTestClasses+3)/2;
    end

    % predictions
    %try
        B = glmfit(outterTrainAttributes,outterTrainClasses,distr,'link',link);
        outterPredictedProba = glmval(B,outterTestAttributes,link);
    %catch err
    %    outterPredictedProba = ones(size(outterTestClasses))*-100;
    %end
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    output = {outterTestIndex,outterTestClasses,outterPredictedProba};

end