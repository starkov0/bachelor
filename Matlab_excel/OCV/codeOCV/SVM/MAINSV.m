% SVM

function MAINSV(autoscale,boxconstraint,kernel,method,mlp1,mlp2,polyorder,sigma,outterTestIndex)

    if not(not(strcmp(kernel,'mlp')) && ((mlp1~=0.5) || (mlp2~=-0.5)))
        if not(not(strcmp(kernel,'polynomial')) && polyorder~=1)
            if not(not(strcmp(kernel,'rbf')) && sigma~=1)
            
                % init data
                Current_dir = './../../dataOCV/SVM/';

                % import data
                data = importfile('./../../data/totalData.csv', 2, 185);
                data(:,23) = dataset(double(data(:,11))./double(data(:,18)));

                % path
                param_name = num2str(outterTestIndex);            
                dir_path = strcat(Current_dir,param_name,'/');
                file_path = strcat(dir_path,'outterCVdata.mat');

                if not(exist(dir_path,'dir'))
                    mkdir(dir_path);
                end

                display(['Start : ',file_path]);

                % check if the file exists
                if not(exist(file_path,'file'))
                    output = computePredictions(data,autoscale,boxconstraint,kernel,method,mlp1,mlp2,polyorder,sigma,outterTestIndex);
                    save(file_path,'output');
                end

                display(['End : ',file_path]);

            end
        end
    end
end
