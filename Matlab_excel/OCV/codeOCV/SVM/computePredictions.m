 % SVM

% computerPredictions Based on Naive Bayes Algorithm
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function output = computePredictions(data,auto,box,kernel,method,mpl1,mlp2,pol,rbf,outterTestIndex)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    % init data
    initDataCompute;

    % outter test - train
    outterTrainIndex = setdiff(outterInstancesList,outterTestIndex);

    % train - test -> INSTANCES
    outterTrainAttributes = double(data(outterTrainIndex,attributes));
    outterTestAttributes = double(data(outterTestIndex,attributes));
    outterTrainClasses = (double(data(outterTrainIndex,classe)));
    outterTestClasses = (double(data(outterTestIndex,classe)));

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % predictions
    %try
        options = optimset('maxiter',1000000);
        B = svmtrain(outterTrainAttributes,outterTrainClasses,'autoscale',auto,'boxconstraint',box,'kernel_function',kernel,'method',method,'mlp_params',[mpl1,mlp2],'polyorder',pol,'rbf_sigma',rbf,'quadprog_opts',options);
        [outterPredictedProba] = svmclassify(B,outterTestAttributes);
    %catch err
    %    outterPredictedProba = ones(size(outterTestClasses))*-100;
    %end
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    output = {outterTestIndex,outterTestClasses,outterPredictedProba};
    
end