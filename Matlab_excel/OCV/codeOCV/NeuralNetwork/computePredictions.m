% NEURAL NETWORK

% computerPredictions Based on Naive Bayes Algorithm
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function output = computePredictions(data,NetType,hiddenSizes,trainFcn,outterTestIndex)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    % init data
    initDataCompute;

    % outter test - train
    outterTrainIndex = setdiff(outterInstancesList,outterTestIndex);

    % train - test -> INSTANCES
    outterTrainAttributes = double(data(outterTrainIndex,attributes));
    outterTestAttributes = double(data(outterTestIndex,attributes));
    outterTrainClasses = (double(data(outterTrainIndex,classe)));
    outterTestClasses = (double(data(outterTestIndex,classe)));

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % modify classes type
    if strcmp(NetType,'cascadeforwardnet')
        net = cascadeforwardnet(hiddenSizes,trainFcn);
    elseif strcmp(NetType,'feedforwardnet')
        net = feedforwardnet(hiddenSizes,trainFcn);
    elseif strcmp(NetType,'fitnet')
        net = fitnet(hiddenSizes,trainFcn);
    elseif strcmp(NetType,'patternnet')
        net = patternnet(hiddenSizes,trainFcn);
    end
    net.trainParam.showWindow=0;

    % predictions
    %try
        net = configure(net,outterTrainAttributes',outterTrainClasses');
        net = train(net,outterTrainAttributes',outterTrainClasses');
        outterPredictedProba = net(outterTestAttributes');
    %catch err
    %    outterPredictedProba = ones(size(outterTestClasses))*-100;
    %end
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    output = {outterTestIndex,outterTestClasses,outterPredictedProba};

end