function displayParameters(title,min_error)

    display(title);
    for i = 1:size(min_error,1)
        display(min_error{i,2});
    end

end