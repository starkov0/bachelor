function file_list = get_file_list(tot_file_list)
    
    bad_file = {'.','..','.DS_Store'};
    dir_tot_file_list = [dir(tot_file_list)];
    file_list = setxor(bad_file,{dir_tot_file_list.name});
    
end