function error = getError(real,predictionsProba)

    uniqueRealValues = unique(real);
    meanUniqueRealValue = (uniqueRealValues(1) + uniqueRealValues(2))/2;
    
    predictionsBinary = predictionsProba > meanUniqueRealValue;
    realBinary = real > meanUniqueRealValue;
    
    errorList = predictionsBinary ~= realBinary;
    error = sum(errorList);
            
end