function writeBestParametersAndOutterIndexes(title,output)

    fid = fopen(title,'w');

    for i_tot = 1:size(output,1)
        paramArray = output{i_tot,2};
        index = output{i_tot,3};
        
        tot = strcat(num2str(cell2mat(paramArray(1))));
        if size(paramArray,2) > 1
            for i_param = 2:size(paramArray,2)
                tot = strcat(tot,{' '},num2str(cell2mat(paramArray(i_param))));
            end
        end
        tot = strcat(tot,{' '});
        
        
        tot = strcat(tot,'[',num2str(index(1)));
        if size(index,2) > 1
            for i_param = 2:size(index,2)
                tot = strcat(tot,',',num2str(index(i_param)));
            end
        end
        tot = strcat(tot,']');
        
        tot= strcat(tot,'\n');
        fprintf(fid,char(tot));
        
    end
    
    fclose(fid);
    
end