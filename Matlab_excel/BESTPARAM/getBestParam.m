%% saved data value
% 1 = index
% 2 = real classes
% 3 = predictions
% 4 = paramters
% 5 = outterTestIndex
%% new data value
% 1 = error
% 2 = parameters
% 3 = outterTestIndex

%% path 
dataICV_path = '../ICV/dataICV/';
paramIndex_path = '../OCV/paramIndexOCV/';

DU_path = 'Dummy/';
NN_path = 'NeuralNetwork/';
NB_path = 'NaiveBayes/';
DT_path = 'DecisionTree/';
LO_path = 'Logistic/';
SV_path = 'SVM/';

DU_txt = 'Dummy.txt';
NN_txt = 'NeuralNetwork.txt';
NB_txt = 'NaiveBayes.txt';
DT_txt = 'DecisionTree.txt';
LO_txt = 'Logistic.txt';
SV_txt = 'SVM.txt';

DU_data_path = [dataICV_path,DU_path];
NN_data_path = [dataICV_path,NN_path];
NB_data_path = [dataICV_path,NB_path];
DT_data_path = [dataICV_path,DT_path];
LO_data_ath = [dataICV_path,LO_path];
SV_data_path = [dataICV_path,SV_path];

DU_paramIndex_txt = [paramIndex_path,DU_txt];
NN_paramIndex_txt = [paramIndex_path,NN_txt];
NB_paramIndex_txt = [paramIndex_path,NB_txt];
DT_paramIndex_txt = [paramIndex_path,DT_txt];
LO_paramIndex_txt = [paramIndex_path,LO_txt];
SV_paramIndex_txt = [paramIndex_path,SV_txt];



%% compute min error
DU_min_error = get_min_error_param_outterIndex_list(DU_data_path);
NN_min_error = get_min_error_param_outterIndex_list(NN_data_path);
NB_min_error = get_min_error_param_outterIndex_list(NB_data_path);
DT_min_error = get_min_error_param_outterIndex_list(DT_data_path);
LO_min_error = get_min_error_param_outterIndex_list(LO_data_ath);
SV_min_error = get_min_error_param_outterIndex_list(SV_data_path);

%% save min error
writeBestParametersAndOutterIndexesDummy(DU_paramIndex_txt,DU_min_error);
writeBestParametersAndOutterIndexes(NN_paramIndex_txt,NN_min_error);
writeBestParametersAndOutterIndexes(NB_paramIndex_txt,NB_min_error);
writeBestParametersAndOutterIndexes(DT_paramIndex_txt,DT_min_error);
writeBestParametersAndOutterIndexes(LO_paramIndex_txt,LO_min_error);
writeBestParametersAndOutterIndexes(SV_paramIndex_txt,SV_min_error);


