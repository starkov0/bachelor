function min_error_param_outterIndex = get_min_error_param_outterIndex_list(algo_path)
    


    listing = get_file_list(algo_path);
    error_param_outterIndex = cell(23,length(listing),3);
    min_error_param_outterIndex = cell(23,3);
    
    
    
    % compute raw error
    for i_dir = 1:length(listing)
        % get file
        output_file = [algo_path,listing{i_dir},'/innerCVdata'];
        if exist([output_file,'.mat'],'file')
            output = load(output_file);
            %compute error
            for i_folds = 1:23
                error_param_outterIndex{i_folds,i_dir,1} = getError(output.output{i_folds,2},output.output{i_folds,3});
                error_param_outterIndex{i_folds,i_dir,2} = output.output{i_folds,4};
                error_param_outterIndex{i_folds,i_dir,3} = output.output{i_folds,5};
            end
        else
            for i_folds = 1:23
                error_param_outterIndex{i_folds,i_dir,1} = 2000;
                error_param_outterIndex{i_folds,i_dir,2} = 'none';
                error_param_outterIndex{i_folds,i_dir,3} = 'none';
            end
        end
    end
        
    % compute optimum error
    for i_index = 1:size(error_param_outterIndex,1)
        %init
        min_error = 1000;
        for i_param = 1:size(error_param_outterIndex,2)
            if error_param_outterIndex{i_index,i_param,1} < min_error
                min_error_param_outterIndex{i_index,1} = error_param_outterIndex{i_index,i_param,1};
                min_error_param_outterIndex{i_index,2} = error_param_outterIndex{i_index,i_param,2};
                min_error_param_outterIndex{i_index,3} = error_param_outterIndex{i_index,i_param,3};
                min_error = error_param_outterIndex{i_index,i_param,1};
            end
        end
    end

end
    

    