function table = get_table(real,v1tmp,v2tmp)

    v1 = real == v1tmp;
    v2 = real == v2tmp;

    table = zeros(2);

    for i=1:size(v1,1)
        if v1(i) == 1 && v2(i) == 1
            table(1,1) = table(1,1) + 1;
        end
        if v1(i) == 1 && v2(i) == 0
            table(1,2) = table(1,2) + 1;
        end
        if v1(i) == 0 && v2(i) == 1
            table(2,1) = table(2,1) + 1;
        end
        if v1(i) == 0 && v2(i) == 0
            table(2,2) = table(2,2) + 1;
        end
    end
end