clear 

%% saved data value
% 1 = index
% 2 = real classes
% 3 = predictions
% 4 = paramters
% 5 = outterTestIndex
%% new data value
% 1 = correct
% 2 = parameters
% 3 = outterTestIndex

%% path 
dataOCV_path = '../OCV/dataOCV/';

DU_path = 'Dummy/';
NN_path = 'NeuralNetwork/';
NB_path = 'NaiveBayes/';
DT_path = 'DecisionTree/';
LO_path = 'Logistic/';
SV_path = 'SVM/';

DU_data_path = [dataOCV_path,DU_path];
NN_data_path = [dataOCV_path,NN_path];
NB_data_path = [dataOCV_path,NB_path];
DT_data_path = [dataOCV_path,DT_path];
LO_data_path = [dataOCV_path,LO_path];
SV_data_path = [dataOCV_path,SV_path];

%% get binary predictions
real_prediction = get_real(DU_data_path);
DU_prediction = get_predictions(DU_data_path);
NN_prediction = get_predictions(NN_data_path);
NB_prediction = get_predictions(NB_data_path);
DT_prediction = get_predictions(DT_data_path);
LO_prediction = get_predictions(LO_data_path);
SV_prediction = get_predictions(SV_data_path);

DU_prediction = DU_prediction > 0.5;
NN_prediction = NN_prediction > 0;
NB_prediction = NB_prediction > 0.5;
DT_prediction = DT_prediction > 0;
LO_prediction = real(LO_prediction) > 1;
SV_prediction = SV_prediction > 0;

% % correct
% display('________CORRECT_________');
% [correctDU,percentDU] = correct(real_prediction,DU_prediction);
% [correctLO,percentLO] = correct(real_prediction,LO_prediction);
% [correctNB,percentNB] = correct(real_prediction,NB_prediction);
% [correctDT,percentDT] = correct(real_prediction,DT_prediction);
% [correctNN,percentNN] = correct(real_prediction,NN_prediction);
% [correctSV,percentSV] = correct(real_prediction,SV_prediction);
% 
% display(['Dummy', ', correct:',num2str(correctDU), ', percent:',num2str(percentDU)]);
% display(['Logistic', ', correct:',num2str(correctLO), ', percent:',num2str(percentLO)]);
% display(['NaiveBayes', ', correct:',num2str(correctNB), ', percent:',num2str(percentNB)]);
% display(['DecisionTree', ', correct:',num2str(correctDT), ', percent:',num2str(percentDT)]);
% display(['Neural', ', correct:',num2str(correctNN), ', percent:',num2str(percentNN)]);
% display(['SVM', ', correct:',num2str(correctSV), ', percent:',num2str(percentSV)]);

%% mcnemar
LO_table = get_table(real_prediction,DU_prediction,LO_prediction)
DT_table = get_table(real_prediction,DU_prediction,DT_prediction)
NB_table = get_table(real_prediction,DU_prediction,NB_prediction)
NN_table = get_table(real_prediction,DU_prediction,NN_prediction)
SV_table = get_table(real_prediction,DU_prediction,SV_prediction)

display('________MCNEMAR_________');
display('Linear regression');
mcnemar(LO_table)
display('Naive Bayes');
mcnemar(NB_table)
display('Decision Tree');
mcnemar(DT_table)
display('Neural Networks');
mcnemar(NN_table)
display('SVM');
mcnemar(SV_table)

%% mcnemar tot
tot_predictions{1} = DU_prediction;
tot_predictions{2} = LO_prediction;
tot_predictions{3} = DT_prediction;
tot_predictions{4} = NB_prediction;
tot_predictions{5} = NN_prediction;
tot_predictions{6} = SV_prediction;



for i=1:size(tot_predictions,2)
    for j=1:size(tot_predictions,2)
        
        table = get_table(real_prediction,tot_predictions{i},tot_predictions{j});
        tot_pvalue(i,j) = mcnemar(table);
        
    end
end

modelsNames = {'Dummy','L. Regression','D. Trees','N. Bayes','N. Nets','SVM'};

figure;
imagesc(tot_pvalue);
set(gca,'XTick', 1:6)
set(gca,'YTick', 1:6)
set(gca,'XTickLabel',modelsNames)
set(gca,'YTickLabel',modelsNames)
set(gca,'FontSize',25)
title1 = title('Models p-values');
 
%% display correctness
% 
% vector = double([percentDU, percentLO, percentNB, percentDT, percentNN, percentSV]);
% 
% fHand = figure;
% aHand = axes('parent', fHand);
% hold(aHand, 'on')
% 
% colors = hsv(6);
% for i = 1:6
%     bar(i,vector(i), 'parent', aHand, 'facecolor', colors(i,:));
% end
% 
% title1 = title('correctness');
% XTickLabel = {'Dummy','L. Regression','D. Tree','N. Bayes','N. Networks','SVM'};
% 
% set(gca,'XTick',[1:6])
% set(gca,'XTickLabel',XTickLabel)
% set(gca,'FontSize',30);
% set(title1,'FontSize',40);















