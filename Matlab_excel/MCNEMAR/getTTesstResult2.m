%% saved data value
% 1 = index
% 2 = real classes
% 3 = predictions
% 4 = paramters
% 5 = outterTestIndex
%% new data value
% 1 = error
% 2 = parameters
% 3 = outterTestIndex


%% path 
dataOCV_path = '../OCV/dataOCV/';

DU_path = 'Dummy/';
NN_path = 'NeuralNetwork/';
NB_path = 'NaiveBayes/';
DT_path = 'DecisionTree/';
LO_path = 'Logistic/';
SV_path = 'SVM/';

DU_data_path = [dataOCV_path,DU_path];
NN_data_path = [dataOCV_path,NN_path];
NB_data_path = [dataOCV_path,NB_path];
DT_data_path = [dataOCV_path,DT_path];
LO_data_path = [dataOCV_path,LO_path];
SV_data_path = [dataOCV_path,SV_path];


%% get binary predictions
real_prediction = get_real(DU_data_path);
DU_prediction = get_predictions(DU_data_path);
NN_prediction = get_predictions(NN_data_path);
NB_prediction = get_predictions(NB_data_path);
DT_prediction = get_predictions(DT_data_path);
LO_prediction = get_predictions(LO_data_path);
SV_prediction = get_predictions(SV_data_path);

DU_prediction = DU_prediction > 0.5;
NN_prediction = NN_prediction > 0;
NB_prediction = NB_prediction > 0.5;
DT_prediction = DT_prediction > 0;
LO_prediction = real(LO_prediction) > 1;
SV_prediction = SV_prediction > 0;

%% compute error

% real
display('________ERROR_________');
error(real_prediction,DU_prediction,'Dummy-Real');
error(real_prediction,LO_prediction,'Logistic-Real');
error(real_prediction,DT_prediction,'DecisionTree-Real');
error(real_prediction,NB_prediction,'NaiveBayes-Real');
error(real_prediction,NN_prediction,'Neural-Real');
error(real_prediction,SV_prediction,'SVM-Real');

%% compute t-test

% real
display('________TTEST-REAL_________');
test(real_prediction,DU_prediction,'Dummy-Real');
test(real_prediction,LO_prediction,'Logistic-Real');
test(real_prediction,DT_prediction,'DecisionTree-Real');
test(real_prediction,NB_prediction,'NaiveBayes-Real');
test(real_prediction,NN_prediction,'Neural-Real');
test(real_prediction,SV_prediction,'SVM-Real');

% dummy
display('________TTEST-DUMMY_________');
test(DU_prediction,LO_prediction,'Logistic-Dummy');
test(DU_prediction,DT_prediction,'DecisionTree-Dummy');
test(DU_prediction,NB_prediction,'NaiveBayes-Dummy');
test(DU_prediction,NN_prediction,'Neural-Dummy');
test(DU_prediction,SV_prediction,'SVM-Dummy');

% LO
display('________TTEST-LO_________');
test(LO_prediction,DU_prediction,'Dummy-LO');
test(LO_prediction,DT_prediction,'DecisionTree-LO');
test(LO_prediction,NB_prediction,'NaiveBayes-LO');
test(LO_prediction,NN_prediction,'Neural-LO');
test(LO_prediction,SV_prediction,'SVM-LO');

% DT
display('________TTEST-DT_________');
test(DT_prediction,DU_prediction,'Dummy-DT');
test(DT_prediction,LO_prediction,'Logistic-DT');
test(DT_prediction,NB_prediction,'NaiveBayes-DT');
test(DT_prediction,NN_prediction,'Neural-DT');
test(DT_prediction,SV_prediction,'SVM-DT');

% NB
display('________TTEST-NB_________');
test(NB_prediction,DU_prediction,'Dummy-NB');
test(NB_prediction,LO_prediction,'Logistic-NB');
test(NB_prediction,DT_prediction,'DecisionTree-NB');
test(NB_prediction,NN_prediction,'Neural-NB');
test(NB_prediction,SV_prediction,'SVM-NB');

% NN
display('________TTEST-NN_________');
test(NN_prediction,DU_prediction,'Dummy-NN');
test(NN_prediction,LO_prediction,'Logistic-NN');
test(NN_prediction,DT_prediction,'DecisionTree-NN');
test(NN_prediction,NB_prediction,'NaiveBayes-NN');
test(NN_prediction,SV_prediction,'SVM-NN');

% SV
display('________TTEST-SV_________');
test(SV_prediction,DU_prediction,'Dummy-SV');
test(SV_prediction,LO_prediction,'Logistic-SV');
test(SV_prediction,DT_prediction,'DecisionTree-SV');
test(SV_prediction,NB_prediction,'NaiveBayes-SV');
test(SV_prediction,NN_prediction,'Neural-SV');

%% display error

tot_predictions = [];
tot_predictions = [tot_predictions real_prediction];
tot_predictions = [tot_predictions DU_prediction];
tot_predictions = [tot_predictions LO_prediction];
tot_predictions = [tot_predictions DT_prediction];
tot_predictions = [tot_predictions NB_prediction];
tot_predictions = [tot_predictions NN_prediction];
tot_predictions = [tot_predictions SV_prediction];

er_vector = ones(1,size(tot_predictions,2)-1);

for i=2:size(tot_predictions,2)
    difference_vector = tot_predictions(:,1) - tot_predictions(:,i);
    error_vector = abs(difference_vector);
    error_sacalar = sum(error_vector);
    percent = error_sacalar / size(tot_predictions(:,i),1);
    er_vector(i-1) = percent;
end


plot(er_vector,'LineWidth',7);
XTickLabel = {'Dummy','L. Regression','D. Tree','N. Bayes','N. Networks','SVM'};
YTick = linspace(0,1,9);
set(gca,'YTick',YTick)
set(gca,'XTickLabel',XTickLabel)
set(gca,'FontSize',40);

%% display ttest

% tot_predictions = [];
% tot_predictions = [tot_predictions real_prediction];
% tot_predictions = [tot_predictions DU_prediction];
% tot_predictions = [tot_predictions LO_prediction];
% tot_predictions = [tot_predictions DT_prediction];
% tot_predictions = [tot_predictions NB_prediction];
% tot_predictions = [tot_predictions NN_prediction];
% tot_predictions = [tot_predictions SV_prediction];
% 
% tt_matrix = eye(size(tot_predictions,2),size(tot_predictions,2));
% 
% for i=1:size(tot_predictions,2)
%     for j=1:size(tot_predictions,2)
%         if i ~= j
%             [h,p]= ttest2(tot_predictions(:,i),tot_predictions(:,j));
% %             if p<0.001
% %                 p = 0;
% %             end
%             tt_matrix(i,j) = p;
%         end
%     end
% end
% 
% figure;
% imagesc(tt_matrix);
% labelsX = {'Real','Dummy','L. Reg.','D. Tree','N. B.','N. Nets','SVM'};
% labelsY = {'Real','Dummy','Logistic','Decision Tree','Naive Bayes','Neural Network','SVM'};
% set(gca, 'XTickLabel', labelsX, 'YTickLabel', labelsY);
% set(gca,'xaxisLocation','top');
% set(gca,'FontSize',40);
% colormap('hot');
