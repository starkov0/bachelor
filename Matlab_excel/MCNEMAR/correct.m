function [correct,percentCorrect] = correct(real,predictions)
    
    difference_vector = real - predictions;
    correct_vector = abs(difference_vector);
    error = sum(correct_vector);
    correct = size(real,1) - error;
    percentCorrect = correct / size(predictions,1);
    
end