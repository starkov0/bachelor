function predictions = get_real(algo_path)
    
    listing = get_file_list(algo_path);
    predictions = zeros(184,1);
    
    % compute raw error
    for i_dir = 1:length(listing)
        % get file
        output = load([algo_path,listing{i_dir},'/outterCVdata']);
        predictions(1+((i_dir-1)*8):(i_dir*8)) = output.output{2};
    end
        
end
    

    