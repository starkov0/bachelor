function [error,percent] = myError(real,predictions)
    
    difference_vector = real - predictions;
    error_vector = abs(difference_vector);
    error = sum(error_vector);
    percent = error / size(predictions,1);
    
end