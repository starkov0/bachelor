#!/bin/bash
cd ..
cd codeICV
cd DecisionTree

counter=0

for merge in 'on' 'off'
do
	for minLeaf in 1 6 11 16 21 26 31 36 41 46
	do
		for minParent in 1 6 11 16 21 26 31 36 41 46
		do
			for prior in 'empirical' 'uniform'
			do
				for prune in 'on' 'off'
				do
					for prunec in 'error' 'impurity'
					do
						for split in 'gdi' 'twoing' 'deviance'
						do
							for surrogate in 'on' 'off'
							do
								if [ $counter -lt 2 ] 
								then
									nohup echo "MAINDT('$merge',$minLeaf,$minParent,'$prior','$prune','$prunec','$split','$surrogate')" | /Applications/Matlab.app/bin/matlab -nodesktop -nosplash -singleCompThread > exec.txt &
								else
									echo "MAINDT('$merge',$minLeaf,$minParent,'$prior','$prune','$prunec','$split','$surrogate')" | /Applications/Matlab.app/bin/matlab -nodesktop -nosplash -singleCompThread > exec.txt
									counter=0
								fi
								counter=$((counter+1))
							done
						done
					done
				done
			done
		done
	done
done

cd ..
cd ..
cd localScript