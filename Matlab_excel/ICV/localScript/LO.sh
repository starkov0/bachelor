#!/bin/bash
cd ..
cd codeICV
cd Logistic

counter=0

for link in 'identity' 'log' 'logit' 'probit' 'comploglog' 'reciprocal' 'loglog' -2
do
	for distr in 'binomial' 'gamma' 'inverse gaussian' 'normal' 'poisson'
	do
		if [ $counter -lt 2 ] 
		then
			nohup echo "MAINLO('$link','$distr')" | /Applications/Matlab.app/bin/matlab -nodesktop -nosplash -singleCompThread > exec.txt &
		else
			echo "MAINLO('$link','$distr')" | /Applications/Matlab.app/bin/matlab -nodesktop -nosplash -singleCompThread > exec.txt
			counter=0
		fi
		counter=$((counter+1))
	done
done

cd ..
cd ..
localScript