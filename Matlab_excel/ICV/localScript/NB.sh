#!/bin/bash
cd ..
cd codeICV
cd NaiveBayes

counter=0

for distribution in 'normal' 'kernel' 'mvmn'
do
	for prior in 'empirical' 'uniform'
	do
		for kssupport in 'unbounded' 'positive'
		do
			for kstype in 'normal' 'box' 'triangle' 'epanechnikov'
			do
				if [ $counter -lt 2 ] 
				then
					nohup echo "MAINNB('$distribution','$prior','$kssupport','$kstype')" | /Applications/Matlab.app/bin/matlab -nodesktop -nosplash -singleCompThread > exec.txt &
				else
					echo "MAINNB('$distribution','$prior','$kssupport','$kstype')" | /Applications/Matlab.app/bin/matlab -nodesktop -nosplash -singleCompThread > exec.txt
					counter=0
				fi
				counter=$((counter+1))
			done
		done
	done
done

cd ..
cd ..
cd localScript