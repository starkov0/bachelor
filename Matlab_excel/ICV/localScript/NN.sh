#!/bin/bash
cd ..
cd codeICV
cd NeuralNetwork

counter=0

for netType in 'cascadeforwardnet' 'feedforwardnet' 'fitnet' 'patternnet'
do
	for hiddenSize in 1 6 11 16 21 26 31 36 41 46
	do
		for trainFct in 'trainlm' 'trainbr' 'trainbfg' 'trainrp' 'trainscg' 'traincgb' 'traincgf' 'traincgp' 'trainoss' 'traingdx' 'traingdm' 'traingd' 'trainru' 'trainr'
		do
			if [ $counter -lt 2 ]
			then
				nohup echo "MAINNN('$netType',$hiddenSize,'$trainFct')" | /Applications/Matlab.app/bin/matlab -nodesktop -nosplash -singleCompThread > exec.txt &
			else
				echo "MAINNN('$netType',$hiddenSize,'$trainFct')" | /Applications/Matlab.app/bin/matlab -nodesktop -nosplash -singleCompThread > exec.txt
				counter=0
			fi
			counter=$((counter+1))
		done
	done
done

cd ..
cd ..
cd localScript