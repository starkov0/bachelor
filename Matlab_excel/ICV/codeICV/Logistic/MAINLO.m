% LOGISTIC REGRESSION

function MAINLO(link,distr)

    % init data
    Current_dir = './../../dataICV/Logistic/';

    % import data
    data = importfile('./../../data/totalData.csv', 2, 185);
    data(:,23) = dataset(double(data(:,11))./double(data(:,18)));

    % path
    param_name = strcat(num2str(link),num2str(distr));
    dir_path = strcat(Current_dir,param_name,'/');
    file_path = strcat(dir_path,'innerCVdata.mat');

    if not(exist(dir_path,'dir'))
        mkdir(dir_path);
    end

    display(['Start : ',file_path]);
                
    % check if the file exists
    if not(exist(file_path,'file'))
        if strcmp(distr,'-2')
            output = computePredictions(data,link,-2);
        else
            output = computePredictions(data,link,distr);
        end
        save(file_path,'output');
    end

    display(['End : ',file_path]);
    
end