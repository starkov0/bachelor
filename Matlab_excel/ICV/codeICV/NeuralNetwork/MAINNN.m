% NEURAL NETWORK

function MAINNN(Type,hiddenSizes,trainFcn)

    % init data
    Current_dir = './../../dataICV/NeuralNetwork/';

    % import data
    data = importfile('./../../data/totalData.csv', 2, 185);
    data(:,23) = dataset(double(data(:,11))./double(data(:,18)));

    % path
    param_name = strcat(num2str(Type),num2str(hiddenSizes),num2str(trainFcn));            
    dir_path = strcat(Current_dir,param_name,'/');
    file_path = strcat(dir_path,'innerCVdata.mat');

    if not(exist(dir_path,'dir'))
        mkdir(dir_path);
    end

    display(['Start : ',file_path]);

    % check if the file exists
    if not(exist(file_path,'file'))
        output = computePredictions(data,Type,hiddenSizes,trainFcn);
        save(file_path,'output');
    end

    display(['End : ',file_path]);

end