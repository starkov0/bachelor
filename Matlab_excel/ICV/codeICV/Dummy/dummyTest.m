function probaPredict = dummyTest(classifier,Y)
    probaPredict = ones(size(Y,1),1)*classifier;
end