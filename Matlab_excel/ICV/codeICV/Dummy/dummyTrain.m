function classifier = dummyTrain(~,Y)
    classifier = sum(Y)/length(Y);
end