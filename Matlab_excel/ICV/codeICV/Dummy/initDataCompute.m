% DECISION TREE CLASSIFICATION

% patients pointers
nbPatients = 23;
nbInstancesPerPatient = 8;
[nbInstances,nbAttributes] = size(data);
outterInstancesList = 1:nbInstances;
outterPatientsMatrix = vec2mat(outterInstancesList,nbInstancesPerPatient);

% attributes - classes
attributes = [2,3,11,12,13,14,15,16,17,23];
classe = 20;

% innerTestIndex - innerTestValues - innerProbaValues -
% innerModelParameters - outterTestIndex
ITestIndex_ITest_IPredictProba_IModelParam_OTestIndex = cell(nbPatients,5);