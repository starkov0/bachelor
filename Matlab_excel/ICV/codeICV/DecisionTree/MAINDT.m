% DECISION TREE CLASSIFICATION

function MAINDT(Merge,MinLeaf,MinParent,prior,Prune,PruneC,Split,Surrogate)

    if not(strcmp(PruneC,'impurity') && strcmp(Split,'twoing'))

        % init data
        Current_dir = './../../dataICV/DecisionTree/';

        % import data
        data = importfile('./../../data/totalData.csv', 2, 185);
        data(:,23) = dataset(double(data(:,11))./double(data(:,18)));

        % path
        param_name = strcat(num2str(Merge),num2str(MinLeaf),num2str(MinParent),num2str(prior),num2str(Prune),num2str(PruneC),num2str(Split),num2str(Surrogate));
        dir_path = strcat(Current_dir,param_name,'/');
        file_path = strcat(dir_path,'innerCVdata.mat');

        if not(exist(dir_path,'dir'))
            mkdir(dir_path);
        end

        display(['Start : ',file_path]);

        % check if the file exists
        if not(exist(file_path,'file'))
            output = computePredictions(data,Merge,MinLeaf,MinParent,prior,Prune,PruneC,Split,Surrogate);
            save(file_path,'output');
        end

        display(['End : ',file_path]);

    end
    
end