% NAIVE BAYES

function MAINNB(Distribution,Prior,KSSupport,KSType)

    % init data
    Current_dir = './../../dataICV/NaiveBayes/';

    % import data
    data = importfile('./../../data/totalData.csv', 2, 185);
    data(:,23) = dataset(double(data(:,11))./double(data(:,18)));

    %path
    param_name = strcat(num2str(Distribution),num2str(Prior),num2str(KSSupport),num2str(KSType))
    dir_path = strcat(Current_dir,param_name,'/');
    file_path = strcat(dir_path,'innerCVdata.mat');

    if not(exist(dir_path,'dir'))
        mkdir(dir_path);
    end

    display(['Start : ',file_path]);

    % check if the file exists
    if not(exist(file_path,'file'))
        output = computePredictions(data,Distribution,Prior,KSSupport,KSType);
        save(file_path,'output');
    end

    display(['End : ',file_path]);

end