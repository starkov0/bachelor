% NAIVE BAYES

% computerPredictions Based on Naive Bayes Algorithm
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function ITestIndex_ITest_IPredictProba_IModelParam_OTestIndex = computePredictions(data,Distr,Prior,KSSupport,KSType)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% init data
initDataCompute;

for outterCV = 1:nbPatients
    % outter test - train
    outterTestIndex = outterPatientsMatrix(outterCV,:);
    outterTrainIndex = setdiff(outterInstancesList,outterTestIndex);

    % inner data sets
    innerInstancesList = outterTrainIndex;
    innerPatientsMatrix = vec2mat(innerInstancesList,nbInstancesPerPatient);

    % data init
    innerTestIndexTotal = zeros(length(innerInstancesList),1);
    innerTestClassesTotal = zeros(length(innerInstancesList),1);
    innerPredictedProbaTotal = zeros(length(innerInstancesList),1);

    for innerCV = 1:nbPatients-1
        % inner test - train
        innerTestIndex = innerPatientsMatrix(innerCV,:);
        innerTrainIndex = setdiff(innerInstancesList,innerTestIndex);

        % train - test -> INSTANCES
        innerTrainAttributes = double(data(innerTrainIndex,attributes));
        innerTestAttributes = double(data(innerTestIndex,attributes));
        innerTrainClasses = (double(data(innerTrainIndex,classe)));
        innerTestClasses = (double(data(innerTestIndex,classe)));    

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % modify classes type
        innerTrainClasses = (innerTrainClasses+1)/2;
        innerTestClasses = (innerTestClasses+1)/2;

        % predictions
        try
            B = NaiveBayes.fit(innerTrainAttributes,innerTrainClasses,'Distribution',Distr,'Prior',Prior,'KSSupport',KSSupport,'KSType',KSType);
            innerPredictedProbaTOT = posterior(B,innerTestAttributes);
            innerPredictedProba = innerPredictedProbaTOT(:,2);
        catch err
            innerPredictedProba = ones(size(innerTestClasses))*-100;
        end
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        % memorize data -> per train
        innerTestIndexTotal((innerCV-1)*nbInstancesPerPatient+1:innerCV*nbInstancesPerPatient) = innerTestIndex;
        innerTestClassesTotal((innerCV-1)*nbInstancesPerPatient+1:innerCV*nbInstancesPerPatient) = innerTestClasses;
        innerPredictedProbaTotal((innerCV-1)*nbInstancesPerPatient+1:innerCV*nbInstancesPerPatient) = innerPredictedProba;                

    end

    % save in local memory
    ITestIndex_ITest_IPredictProba_IModelParam_OTestIndex{outterCV,1} = innerTestIndexTotal;
    ITestIndex_ITest_IPredictProba_IModelParam_OTestIndex{outterCV,2} = innerTestClassesTotal;
    ITestIndex_ITest_IPredictProba_IModelParam_OTestIndex{outterCV,3} = innerPredictedProbaTotal;
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    ITestIndex_ITest_IPredictProba_IModelParam_OTestIndex{outterCV,4} = {Distr,Prior,KSSupport,KSType};
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    ITestIndex_ITest_IPredictProba_IModelParam_OTestIndex{outterCV,5} = outterTestIndex;

end