#!/bin/bash
cd ..
cd codeICV
cd SVM

counter=0

for autoscale in 0 1
do
	for boxconstraint in 1 6 11 16 21 26 
	do
		for kernel in 'linear' 'quadratic' 'polynomial' 'rbf' 'mlp'
		do
			for method in 'QP'
			do
				for mlp1 in 0.5 2.5 4.5 6.5 8.5
				do
					for mlp2 in -8.5 -6.5 -4.5 -2.5 -0.5
					do
						for polyorder in 1 4 7
						do
							for sigma in 1 4 7
							do
								if [ $counter -lt 30 ] 
								then
									nohup srun echo "MAINSV($autoscale,$boxconstraint,'$kernel','$method',$mlp1,$mlp2,$polyorder,$sigma)" | /opt/uau/matlab714/bin/matlab -nodesktop -nosplash -singleCompThread > exec.txt &
								else
									srun echo "MAINSV($autoscale,$boxconstraint,'$kernel','$method',$mlp1,$mlp2,$polyorder,$sigma)" | /opt/uau/matlab714/bin/matlab -nodesktop -nosplash -singleCompThread > exec.txt
									counter=0
								fi
								counter=$((counter+1))
							done
						done
					done
				done
			done
		done
	done
done

cd ..
cd ..
cd omuraScript