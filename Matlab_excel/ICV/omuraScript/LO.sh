#!/bin/bash
cd ..
cd codeICV
cd Logistic

counter=0

for link in 'identity' 'log' 'logit' 'probit' 'comploglog' 'reciprocal' 'loglog' -2
do
	for distr in 'binomial' 'gamma' 'inverse gaussian' 'normal' 'poisson'
	do
		if [ $counter -lt 30 ]
		then
			nohup srun echo "MAINLO('$link','$distr')" | /opt/uau/matlab714/bin/matlab -nodesktop -nosplash -singleCompThread > exec.txt &
		else
			srun echo "MAINLO('$link','$distr')" | /opt/uau/matlab714/bin/matlab -nodesktop -nosplash -singleCompThread > exec.txt
			counter=0
		fi
		counter=$((counter+1))
	done
done

cd ..
cd ..
cd omuraScript