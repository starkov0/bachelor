% load
pwd = '/Users/pierrestarkov/Desktop/BACHELOR/Matlab_DeepLearning/myCode/';
data = load([pwd,'5_data/linked.mat']);
image_vector_tot = data.image_vector_tot;
patho_vector_tot = data.patho_vector_tot;

% subsample
imgResized = zeros(28,28,size(image_vector_tot,3));
for i=1:size(data.image_vector_tot,3)
    imgResized(:,:,i) = imresize(image_vector_tot(:,:,i),[28,28]);
end
image_vector_tot = imgResized;

%save
save([pwd,'5_data/subsampled.mat'],'image_vector_tot','patho_vector_tot','-v7.3');