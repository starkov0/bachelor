function [er, bad, proba, net] = cnntest(net, x, y)
    %  feedforward
    net = cnnff(net, x);
    [~, h] = max(net.o);
    [~, a] = max(y);
    bad = find(h ~= a);
    
    proba = net.o;
    
    er = numel(bad) / size(y, 2);
end