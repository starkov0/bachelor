#!/bin/bash

#SBATCH --ntasks-per-node=10
#SBATCH --ntasks-per-core=10
#SBATCH --mem=10000
#SBATCH --ntasks=3

srun echo "cnnMain(${1})" | /opt/uau/matlab714/bin/matlab -nodesktop -nosplash -singleCompThread > exec.txt

