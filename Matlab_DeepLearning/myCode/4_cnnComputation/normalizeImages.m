function [train_x,test_x] = normalizeImages(train_x,test_x,totalImages)
    % compute min max
    minValue = abs(min(totalImages(:)));
    maxValue = max(totalImages(:));
    
    % compute additive matrices
    tmpTrain = train_x ~= 0;
    tmpTrain = tmpTrain * minValue;
    tmpTest = test_x ~= 0;
    tmpTest = tmpTest * minValue;
    
    % normalize
    train_x = (train_x + tmpTrain) / maxValue;
    test_x = (test_x + tmpTest) / maxValue;
end