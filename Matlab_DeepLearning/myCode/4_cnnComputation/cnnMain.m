function cnnMain(batchsize,alpha,numepochs,kernelsize,outputmaps,scale)
    
    pwd = '../5_data/';
    TEST = testCNNArchitecture(256,kernelsize,scale);

    if TEST == 1
        
        display('load data');
        %load data
        data = loadData(pwd,'linkedEqualAdded');
        
        display('normalize data');
        % normalize data
        data.image_vector_tot = normalizeImages2(data.image_vector_tot);
        
        display('create train - test data');
        % create train-test-x-y data
        image_nb = size(data.image_vector_tot,3);
        image_nb_deuxTiers = round(image_nb/3)*2;
        image_nb_deuxTiers = batchsize * round(image_nb_deuxTiers/batchsize);
        
        train_x = double(data.image_vector_tot(:,:,1:image_nb_deuxTiers));
        test_x = double(data.image_vector_tot(:,:,image_nb_deuxTiers+1:end));
        train_y = double(data.patho_vector_tot(:,1:image_nb_deuxTiers));
        test_y = double(data.patho_vector_tot(:,image_nb_deuxTiers+1:end));

        % create options
        opts.alpha = alpha;
        opts.batchsize = batchsize;
        opts.numepochs = numepochs;
        
        display('create net');

        % setup cnn
        cnn = createCNNArchitecture(kernelsize,outputmaps,scale);
        cnn = cnnsetup(cnn, train_x, train_y);

        display('train net');

        % train
        cnn = cnntrain(cnn, train_x, train_y, opts);

        display('test net');

        % test
        [er, bad, proba, cnn] = cnntest(cnn, test_x, test_y);
        
        % SAVE
        argumentsFile = [];
        
        % batchsize
        argumentsFile = [argumentsFile,num2str(batchsize),','];
        
        % alpha
        argumentsFile = [argumentsFile,num2str(alpha),','];
        
        % numepochs
        argumentsFile = [argumentsFile,num2str(numepochs),','];
        
        % kernelsize
        for i=1:size(kernelsize,2)
            argumentsFile = [argumentsFile,num2str(kernelsize(i)),','];
        end
        
        % outputmaps
        for i=1:size(outputmaps,2)
            argumentsFile = [argumentsFile,num2str(outputmaps(i)),','];
        end
        
        % scale
        for i=1:size(scale,2)
            argumentsFile = [argumentsFile,num2str(scale(i)),','];
        end
        
        display(argumentsFile);
        save(['../8_results/',argumentsFile], 'er', 'bad', 'proba', 'cnn', 'test_y');
        
        display('end');

    end

end
