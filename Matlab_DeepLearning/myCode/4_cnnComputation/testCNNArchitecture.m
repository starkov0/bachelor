%% test CNN architecture
%     for convolution layer :
%         mapsize = mapsize - kernelsize + 1 -> must be greater than 0
%     for subsampling layer :
%         mapsize = mapsize / scale -> must be  positiv integer number
%     
%     numberOfImages / batchsize -> must be  positiv integer number
function TEST = testCNNArchitecture(mapsize,kernelsize,scale)

    TEST = 1;
    sizeCNN = length(kernelsize);
    
    for i = 1:sizeCNN
        
        display(['kernelsize: ',num2str(kernelsize(i)),', i: ',num2str(i)]);
        display([num2str(mapsize - kernelsize(i) + 1),' = ',num2str(mapsize),' - ',num2str(kernelsize(i)),' + 1']);
        mapsize = mapsize - kernelsize(i) + 1;
        if mapsize <= 0
            display('kernelsize error');
            TEST = 0;
        end
        
        display([num2str(mapsize / scale(i)),' = ',num2str(mapsize),' / ',num2str(scale(i))]);
        mapsize = mapsize / scale(i);
        if mapsize <= 0
            display('scale error 1');
            TEST = 0;
        end
        if mapsize ~= round(mapsize)
            display('scale error 2');
            TEST = 0;
        end
        display(['_']);
        
    end

end