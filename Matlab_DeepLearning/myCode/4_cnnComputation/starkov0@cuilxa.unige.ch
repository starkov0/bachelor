%function cnnImagingPredictions(batchsize,alpha,numepochs,kernelsize,outputmaps,scale)
    
    batchsize = 40;
    alpha = 1;
    numepochs = 10;
    kernelsize = [5,5,4,4];
    outputmaps = [6,12,12,6];
    scale = [2,2,2,2];
    
    pwd = '../5_data/';
    TEST = testCNNArchitecture(256,kernelsize,scale);

    if TEST == 1
        
        display('load data');
        %load data
        data = loadData(pwd,'linkedAdded');
        
        display('normalize data');
        % normalize data
        data.image_vector_tot = normalizeImages2(data.image_vector_tot);
        
        display('create train - test data');
        % create train-test-x-y data
        image_nb = size(data.image_vector_tot,3);
        image_nb_deuxTiers = round(image_nb/3)*2;
        image_nb_deuxTiers = batchsize * round(image_nb_deuxTiers/batchsize);
        
        train_x = double(data.image_vector_tot(:,:,1:image_nb_deuxTiers));
        test_x = double(data.image_vector_tot(:,:,image_nb_deuxTiers+1:end));
        train_y = double(data.patho_vector_tot(:,1:image_nb_deuxTiers));
        test_y = double(data.patho_vector_tot(:,image_nb_deuxTiers+1:end));

        % create options
        opts.alpha = alpha;
        opts.batchsize = batchsize;
        opts.numepochs = numepochs;
        
        display('create net');

        % setup cnn
        cnn = createCNNArchitecture(kernelsize,outputmaps,scale);
        cnn = cnnsetup(cnn, train_x, train_y);

        display('train net');

        % train
        cnn = cnntrain(cnn, train_x, train_y, opts);

        display('test net');

        % test
        [er, bad, proba] = cnntest(cnn, test_x, test_y);
        display(['error % : ', num2str(er)]);
        display('proba: ');
        display(num2str(proba));
        
        % plot mean squared error
        %figure; plot(cnn.rL);
        %[~, h] = max(proba);
        
        display('end');

    end

%end
