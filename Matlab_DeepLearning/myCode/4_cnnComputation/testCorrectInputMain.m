kernelsize = [];
scale = [];
outputmaps = [];
var = 1;
modVar = 1000;



for batchsize=20:30:50
    for alpha=1:5:10
        for numepochs=20
            
            for kernel1=2:10
                for scale1=2
                    for outputpmaps1 = 3:5:15
                        
                        for kernel2=2:10
                            for scale2=2
                                for outputpmaps2 = 3:5:15
                                    
                                    kernelsize = [kernel1,kernel2];
                                    outputmaps = [outputpmaps1,outputpmaps2];
                                    scale = [scale1,scale2];
                                    var = var + 1;
                                    if mod(var, modVar) == 0
                                        testCorrectInputWriteInFile(batchsize,alpha,numepochs,kernelsize,outputmaps,scale);
                                    end
                                    
                                    for kernel3=2:10
                                        for scale3=2
                                            for outputpmaps3 = 3:5:15
                                                
                                                kernelsize = [kernel1,kernel2,kernel3];
                                                outputmaps = [outputpmaps1,outputpmaps2,outputpmaps3];
                                                scale = [scale1,scale2,scale3];
                                                var = var + 1;
                                                if mod(var, modVar) == 0
                                                    testCorrectInputWriteInFile(batchsize,alpha,numepochs,kernelsize,outputmaps,scale);
                                                end
                                                
                                                for kernel4=2:10
                                                    for scale4=2
                                                        for outputpmaps4 = 3:5:15
                                                            
                                                            kernelsize = [kernel1,kernel2,kernel3,kernel4];
                                                            outputmaps = [outputpmaps1,outputpmaps2,outputpmaps3,outputpmaps4];
                                                            scale = [scale1,scale2,scale3,scale4];
                                                            var = var + 1;
                                                            if mod(var, modVar) == 0
                                                                testCorrectInputWriteInFile(batchsize,alpha,numepochs,kernelsize,outputmaps,scale);
                                                            end
                                                            
                                                            for kernel5=2:10
                                                                for scale5=2
                                                                    for outputpmaps5 = 3:5:15
                                                                        
                                                                        kernelsize = [kernel1,kernel2,kernel3,kernel4,kernel5];
                                                                        outputmaps = [outputpmaps1,outputpmaps2,outputpmaps3,outputpmaps4,outputpmaps5];
                                                                        scale = [scale1,scale2,scale3,scale4,scale5];
                                                                        var = var + 1;
                                                                        if mod(var, modVar) == 0
                                                                            testCorrectInputWriteInFile(batchsize,alpha,numepochs,kernelsize,outputmaps,scale);
                                                                        end
                                                                        
                                                                    end
                                                                end
                                                            end
                                                        end
                                                    end
                                                end
                                            end
                                        end
                                    end
                                end
                            end
                        end
                    end
                end
            end
            
        end
    end
end