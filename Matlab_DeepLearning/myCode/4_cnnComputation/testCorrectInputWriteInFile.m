function testCorrectInputWriteInFile(batchsize,alpha,numepochs,kernelsize,outputmaps,scale)
    
    result_dir = '../8_results/';
    TEST = testCNNArchitecture(256,kernelsize,scale);
    arguments = [];
    
    if TEST == 1
        
        % batchsize
        arguments = [arguments,num2str(batchsize),','];
        
        % alpha
        arguments = [arguments,num2str(alpha),','];
        
        % numepochs
        arguments = [arguments,num2str(numepochs),','];
        
        % kernelsize
        arguments = [arguments,'['];
        for i=1:size(kernelsize,2)
            arguments = [arguments,num2str(kernelsize(i)),','];
        end
        arguments = arguments(1:size(arguments,2)-1);
        arguments = [arguments, '],'];
        
        % outputmaps
        arguments = [arguments,'['];
        for i=1:size(outputmaps,2)
            arguments = [arguments,num2str(outputmaps(i)),','];
        end
        arguments = arguments(1:size(arguments,2)-1);
        arguments = [arguments, '],'];
        
        % scale
        arguments = [arguments,'['];
        for i=1:size(scale,2)
            arguments = [arguments,num2str(scale(i)),','];
        end
        arguments = arguments(1:size(arguments,2)-1);
        arguments = [arguments, ']'];
        
        % end
        arguments = [arguments, '\n'];
        
        % write in file
        fid = fopen('correctInput.txt','a');
        fprintf(fid, arguments);
        fclose(fid);
        
    end
    
end
