function cnn = createCNNArchitecture(kernelsize,outputmaps,scale)

    rand('state',0)
    
    cnn.layers = {};
    cnn.layers{1} = struct('type', 'i'); %input layer
    index = 2;

    for i=1:length(kernelsize)
        cnn.layers{index} = struct('type', 'c', 'outputmaps', outputmaps(i), 'kernelsize', kernelsize(i)); %convolution layer
        index = index + 1;
        cnn.layers{index} = struct('type', 's', 'scale', scale(i)); %subsampling layer
        index = index + 1;
    end

end