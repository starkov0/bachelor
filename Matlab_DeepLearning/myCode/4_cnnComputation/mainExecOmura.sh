#!/bin/bash

#SBATCH --ntasks-per-node=3

while read line           
do
    sbatch execOmura.sh ${line}
done < correctInput.txt
