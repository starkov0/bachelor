#!/bin/bash

counter=0

while read line           
do
    if [ $counter -lt 50 ]
    then
        nohup srun echo $PATH > exec.txt &
    else
        srun echo $PATH > exec.txt
        counter=0
    fi
    counter=$((counter+1))
done < correctInput.txt
