function imageRect = getImageRect(image_file)

    image = dicomread(image_file);
    [h,w] = size(image);

    imageRect.x = 0;
    imageRect.y = 0;
    imageRect.h = h;
    imageRect.w = w;

    % get X
    i = 0;
    image_vector_bin_sum = 0;
    while (i <= w) && (image_vector_bin_sum == 0)
        i = i+1;
        image_vector_bin = image(:,i) ~= 0;
        image_vector_bin_sum = sum(image_vector_bin(:));
    end
    imageRect.x = i;
    
    % get Y
    i = 0;
    image_vector_bin_sum = 0;
    while (i <= h) && (image_vector_bin_sum == 0)
        i = i+1;
        image_vector_bin = image(i,:) ~= 0;
        image_vector_bin_sum = sum(image_vector_bin(:));
    end
    imageRect.y = i;

    % get W
    i = w+1;
    image_vector_bin_sum = 0;
    while (i >= 0) && (image_vector_bin_sum == 0)
        i = i-1;
        image_vector_bin = image(:,i) ~= 0;
        image_vector_bin_sum = sum(image_vector_bin(:));
    end
    imageRect.w = i - imageRect.x;

    % get H
    i = h+1;
    image_vector_bin_sum = 0;
    while (i >= 0) && (image_vector_bin_sum == 0)
        i = i-1;
        image_vector_bin = image(i,:) ~= 0;
        image_vector_bin_sum = sum(image_vector_bin(:));
    end
    imageRect.h = i - imageRect.y;

end