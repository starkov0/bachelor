
widthMax = 256;
heightMax = 256;

segm_dir = '/Users/pierrestarkov/Desktop/BACHELOR/Data/SEGMENTATIONNOMS/';
segm_dir_list = dir(segm_dir);

segm_dir_list_size = size(segm_dir_list,1);
for patient_id=1:segm_dir_list_size
    if ~strcmp('.',segm_dir_list(patient_id).name) && ~strcmp('..',segm_dir_list(patient_id).name) && ~strcmp('.DS_Store',segm_dir_list(patient_id).name)
        patient_dir = [segm_dir,segm_dir_list(patient_id).name,'/'];
        createDir(patient_dir);
        %display(patient_dir);
        patient_dir_list = dir(patient_dir);
        patient_dir_list_size = size(patient_dir_list,1);
        
        for exam_id=1:patient_dir_list_size
            if ~strcmp('.',patient_dir_list(exam_id).name) && ~strcmp('..',patient_dir_list(exam_id).name) && ~strcmp('.DS_Store',patient_dir_list(exam_id).name)
                exam_dir = [patient_dir,patient_dir_list(exam_id).name,'/'];
                createDir(exam_dir);
                %display(exam_dir);
                exam_dir_list = dir(exam_dir);
                exam_dir_list_size = size(exam_dir_list,1);
                
                for image_id=1:exam_dir_list_size
                    if ~strcmp('.',exam_dir_list(image_id).name) && ~strcmp('..',exam_dir_list(image_id).name) && ~strcmp('.DS_Store',exam_dir_list(image_id).name)
                        image_file = [exam_dir,exam_dir_list(image_id).name];
                        display(image_file);
                        %deleteUnNeededImage(image_file);
                        %imageRect = getImageRect(image_file);
                        %[widthMax, heightMax] = getImageRectMaxWidthHeight(imageRect,widthMax,heightMax);
                        
                        imageRect = getImageRect(image_file);
                        imageCenteroide = getImageCenteroide(imageRect);
                        image = dicomread(image_file);
                        imageCrop = getImageCrop(image,imageCenteroide,widthMax,heightMax);
                        saveImageCrop(imageCrop,image_file);
                        
                    end
                end
            end 
        end 
    end
end

