function imageCenteroide = getImageCenteroide(imageRect)

    imageCenteroide.x = imageRect.x + round(imageRect.w/2);
    imageCenteroide.y = imageRect.y + round(imageRect.h/2);

end