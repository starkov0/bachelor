function saveImageCrop(imageCrop, image_file)

    start_dir = image_file(1:43);
    imageCrop_name = 'IMAGECROPNOMS';
    end_dir = image_file(60:end);

    imageCrop_file = [start_dir,imageCrop_name,end_dir];

    dicomwrite(imageCrop,imageCrop_file);

end