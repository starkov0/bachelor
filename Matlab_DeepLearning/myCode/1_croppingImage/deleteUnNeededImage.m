function deleteUnNeededImage(image_file)
    
    image = dicomread(image_file);
    image_bin = image ~= 0;
    image_bin_sum = sum(image_bin(:));
    if image_bin_sum == 0
        display(image_file);
        delete(image_file);
    end

end