function [widthMax,heightMax] = getImageRectMaxWidthHeight(imageRect,width,height)

    widthMax = max(imageRect.w,width);
    heightMax = max(imageRect.h,height);

end