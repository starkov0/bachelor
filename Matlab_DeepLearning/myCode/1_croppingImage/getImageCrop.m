function imageCrop = getImageCrop(image,imageCenteroide,widthMax,heightMax)

    halfW = round(widthMax/2);
    halfH = round(heightMax/2);
    
    imageCrop = image(imageCenteroide.y-halfH:imageCenteroide.y+halfH-1,imageCenteroide.x-halfW:imageCenteroide.x+halfW-1);

end