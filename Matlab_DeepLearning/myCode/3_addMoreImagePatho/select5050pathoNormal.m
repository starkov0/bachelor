% load data
pwd = '../5_data/';
data = load([pwd,'linked.mat']);
image_vector_tot = data.image_vector_tot;
patho_vector_tot = data.patho_vector_tot;

% number of elements to take
nb1 = sum(patho_vector_tot(1,:) == 1);
nb2 = sum(patho_vector_tot(2,:) == 1);
nbEqual = min(nb1,nb2);

% find positions
position1 = find(patho_vector_tot(1,:) == 1);
position2 = find(patho_vector_tot(2,:) == 1);

% final position
position1equal = position1(1:nbEqual);
position2equal = position2(1:nbEqual);
positionequal = [position1equal,position2equal];

% prepare image - patho
image_vector_tot = image_vector_tot(:,:,positionequal);
patho_vector_tot = patho_vector_tot(:,positionequal);

% randomize
randomize = randperm(size(image_vector_tot,3));
image_vector_tot = image_vector_tot(:,:,randomize);
patho_vector_tot = patho_vector_tot(:,randomize);

save([pwd,'linkedEqual.mat'],'image_vector_tot','patho_vector_tot');