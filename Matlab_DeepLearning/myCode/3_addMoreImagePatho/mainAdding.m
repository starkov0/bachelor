% load data
pwd = '../5_data/';
data = load([pwd,'linkedEqual.mat']);
image_vector_tot = data.image_vector_tot;
patho_vector_tot = data.patho_vector_tot;

% randomize
randomize = randperm(size(image_vector_tot,3));
image_vector_tot = image_vector_tot(:,:,randomize);
patho_vector_tot = patho_vector_tot(:,randomize);

for i=1:size(image_vector_tot,3)
%     
    % init
    image_vector_tot = data.image_vector_tot;
    patho_vector_tot = data.patho_vector_tot;
    
    image_vector_tmp = image_vector_tot(:,:,i);
    patho_vector_tmp = patho_vector_tot(:,i);
    
    display([num2str(i),' / ', num2str(size(image_vector_tot,3))]);

    % compute rotation
    [image_vector_tot,patho_vector_tot] = rotateImagePathoTotal(image_vector_tmp,patho_vector_tmp);

    % save data
    save([pwd,'linkedEqualAdded',num2str(i),'.mat'],'image_vector_tot','patho_vector_tot');
end