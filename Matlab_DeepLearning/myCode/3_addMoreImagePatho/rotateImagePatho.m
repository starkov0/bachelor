function [imageRotatedSet,pathoRotatedSet] = rotateImagePatho(image,patho)

    [l,h] = size(image);
    angle = 1:5:360;
    imageRotatedSet = zeros(l,h,length(angle));
    pathoRotatedSet = zeros(2,length(angle));
    
    for i=1:length(angle)
        imageRotated = imrotate(image,angle(i));
        imageRotatedScaled = imresize(imageRotated,[l,h]);
        
        imageRotatedSet(:,:,i) = imageRotatedScaled;
        pathoRotatedSet(:,i) = patho;
    end
    
end