function [imageRotatedSetTotal,pathoRotatedSetTotal] = rotateImagePathoTotal(imageSet,pathoSet)

    imageRotatedSetTotal = [];
    pathoRotatedSetTotal = [];
    nbImage = size(imageSet,3);
    index = 1;
    
    for i=1:nbImage
        
        % display(['image: ', num2str(i), ' -> total: ', num2str(nbImage)]);
        
        [imageRotatedSet,pathoRotatedSet] = rotateImagePatho(imageSet(:,:,i),pathoSet(:,i));
        
        for j=1:size(imageRotatedSet,3)
            imageRotatedSetTotal(:,:,index) = imageRotatedSet(:,:,j);
            pathoRotatedSetTotal(:,index) = pathoRotatedSet(:,j);
            index = index + 1;
        end
        
    end

end