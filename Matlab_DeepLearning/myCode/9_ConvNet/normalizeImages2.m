function totalImages = normalizeImages2(totalImages)

    % compute min
    minValue = abs(min(min(totalImages)));
    
    % init additive matrices
    tmpImages = double(totalImages ~= 0);

    % compute additive matrices
    for i=1:size(totalImages,3)
        tmpImages(:,:,i) = tmpImages(:,:,i) * minValue(i);
    end
    
    % add additive matrices
    totalImages = (totalImages + tmpImages);
        
    % compute max
    maxValue = max(max(totalImages));

    % divide 
    for i=1:size(totalImages,3)
        totalImages(:,:,i) = totalImages(:,:,i) / maxValue(i);
    end
    
end