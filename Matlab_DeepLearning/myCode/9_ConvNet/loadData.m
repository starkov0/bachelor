function dataTot = loadData(pwd, filename)

    imageData = [];
    pathoData = [];
    content = dir(pwd);
    totPointer = 1;
    
    for i=1:size(content,1)
        findName = strfind(content(i).name,filename);
        if size(findName,1) == 1
            pwdTot = [pwd,content(i).name];
            display(pwdTot);
            data = load(pwdTot);
            for j=1:size(data.image_vector_tot,3)
                imageData(:,:,totPointer) = data.image_vector_tot(:,:,j);
                pathoData(:,totPointer) = data.patho_vector_tot(:,j);
                totPointer = totPointer + 1;
            end
        end
    end
    
    dataTot.image_vector_tot = imageData;
    dataTot.patho_vector_tot = pathoData;
    
end