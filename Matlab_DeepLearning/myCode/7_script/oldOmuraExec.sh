#!/bin/bash

counter=0

while read line           
do
    if [ $counter -lt 30 ]
    then
        nohup srun echo "cnnMain(${line})" | /opt/uau/matlab714/bin/matlab -nodesktop -nosplash -singleCompThread > exec.txt &
    else
        srun echo "cnnMain(${line})" | /opt/uau/matlab714/bin/matlab -nodesktop -nosplash -singleCompThread > exec.txt
        counter=0
    fi
    counter=$((counter+1))
done < correctInput.txt
