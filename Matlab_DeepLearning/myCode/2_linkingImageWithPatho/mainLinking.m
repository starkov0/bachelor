% data
image_vector_tot = [];
patho_vector_tot = [];
vector_tot_id = 1;

% patho
patho_file = '/Users/pierrestarkov/Desktop/BACHELOR/Data/excel/totalData.csv';
patho =  importfile(patho_file,1, 185);

%image
segm_dir = '/Users/pierrestarkov/Desktop/BACHELOR/Data/IMAGECROPNOMS/';
segm_dir_list = dir(segm_dir);

segm_dir_list_size = size(segm_dir_list,1);
for patient_id=1:segm_dir_list_size
    if ~strcmp('.',segm_dir_list(patient_id).name) && ~strcmp('..',segm_dir_list(patient_id).name) && ~strcmp('.DS_Store',segm_dir_list(patient_id).name)
        patient_dir = [segm_dir,segm_dir_list(patient_id).name,'/'];
        patient_dir_list = dir(patient_dir);
        patient_dir_list_size = size(patient_dir_list,1);
        
        for exam_id=1:patient_dir_list_size
            if ~strcmp('.',patient_dir_list(exam_id).name) && ~strcmp('..',patient_dir_list(exam_id).name) && ~strcmp('.DS_Store',patient_dir_list(exam_id).name)
                exam_dir = [patient_dir,patient_dir_list(exam_id).name,'/'];
                patient_name = segm_dir_list(patient_id).name;
                display(patient_name);
                segment_name = patient_dir_list(exam_id).name;
                [image_vector,patho_vector] = getPathoForImageDir(patient_name,segment_name,exam_dir,patho);
                vector_size = size(patho_vector,2);
                for i=1:vector_size
                    image_vector_tot(:,:,vector_tot_id) = image_vector(:,:,vector_size);
                    vector_tot_id = vector_tot_id + 1;
                end
                patho_vector_tot = [patho_vector_tot patho_vector];
            end 
        end 
    end
end

save('../5_data/linked.mat','image_vector_tot','patho_vector_tot');
