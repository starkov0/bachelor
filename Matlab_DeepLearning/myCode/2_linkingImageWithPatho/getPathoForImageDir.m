function [image_vector,patho_vector] = getPathoForImageDir(patient_name,segment_name,exam_dir,patho)

    image_vector = [];
    patient_id = 0;
    patient_strfind_size = 0;

    % get patient in patho data
    while patient_strfind_size == 0
        patient_id = patient_id + 1;
        patient_strfind = strfind(patho(patient_id,1),patient_name);
        patient_strfind_size = size(cell2mat(patient_strfind),1);
    end

    % get images
    i = 1;
    exam_dir_list = dir(exam_dir);
    exam_dir_list_size = size(exam_dir_list,1);
    for image_id = 1:exam_dir_list_size
        if ~strcmp('.',exam_dir_list(image_id).name) && ~strcmp('..',exam_dir_list(image_id).name) && ~strcmp('.DS_Store',exam_dir_list(image_id).name)
            image_file = [exam_dir,exam_dir_list(image_id).name];
            image = dicomread(image_file);
            image_vector(:,:,i) = image;
            i = i + 1;
        end
    end

    % get patho segment
    PL = size(strfind(segment_name,'PeriphL'),1);
    PR = size(strfind(segment_name,'PeriphR'),1);
    ZL = size(strfind(segment_name,'ZTL'),1);
    ZR = size(strfind(segment_name,'ZTR'),1);

    image_nb = size(image_vector,3);
    patho_vector = zeros(2,image_nb);

    % ZR
    if ZR > 0 
        patho_strfind = strfind(cell2mat(patho(patient_id+6,3)),'-');
        patho_strfind_size = size(patho_strfind,1);
        if patho_strfind_size == 0
            patho_vector(1,1:image_nb) = 1;
        else
            patho_vector(2,1:image_nb) = 1;
        end
    end

    % ZL
    if ZL > 0 
        patho_strfind = strfind(cell2mat(patho(patient_id+7,3)),'-');
        patho_strfind_size = size(patho_strfind,1);
        if patho_strfind_size == 0
            patho_vector(1,1:image_nb) = 1;
        else
            patho_vector(2,1:image_nb) = 1;
        end
    end

    %PR
    if PR > 0
        image_nb_part = round(image_nb/3);

        patho_strfind_1 = strfind(cell2mat(patho(patient_id+0,3)),'-');
        patho_strfind_2 = strfind(cell2mat(patho(patient_id+2,3)),'-');
        patho_strfind_3 = strfind(cell2mat(patho(patient_id+4,3)),'-');

        patho_strfind_1_size = size(patho_strfind_1,1);
        patho_strfind_2_size = size(patho_strfind_2,1);
        patho_strfind_3_size = size(patho_strfind_3,1);

        if patho_strfind_1_size == 0
            patho_vector(1,1:image_nb_part) = 1;
        else
            patho_vector(2,1:image_nb_part) = 1;
        end

        if patho_strfind_2_size == 0
            patho_vector(1,image_nb_part+1:2*image_nb_part) = 1;
        else
            patho_vector(2,image_nb_part+1:2*image_nb_part) = 1;
        end

        if patho_strfind_3_size == 0
            patho_vector(1,2*image_nb_part+1:end) = 1;
        else
            patho_vector(2,2*image_nb_part+1:end) = 1;
        end
    end

    %PL
    if PL > 0
        image_nb_part = round(image_nb/3);

        patho_strfind_1 = strfind(cell2mat(patho(patient_id+1,3)),'-');
        patho_strfind_2 = strfind(cell2mat(patho(patient_id+3,3)),'-');
        patho_strfind_3 = strfind(cell2mat(patho(patient_id+5,3)),'-');

        patho_strfind_1_size = size(patho_strfind_1,1);
        patho_strfind_2_size = size(patho_strfind_2,1);
        patho_strfind_3_size = size(patho_strfind_3,1);

        if patho_strfind_1_size == 0
            patho_vector(1,1:image_nb_part) = 1;
        else
            patho_vector(2,1:image_nb_part) = 1;
        end

        if patho_strfind_2_size == 0
            patho_vector(1,image_nb_part+1:2*image_nb_part) = 1;
        else
            patho_vector(2,image_nb_part+1:2*image_nb_part) = 1;
        end

        if patho_strfind_3_size == 0
            patho_vector(1,2*image_nb_part+1:end) = 1;
        else
            patho_vector(2,2*image_nb_part+1:end) = 1;
        end
    end 
    
end








